Design 4 IMU
------------
Created by: William O'Connell (s3330253)
-----------

This reposiitory contains the source code for the sensor fusion algorithm
created by Sofia Suvorova ported from MATLAB to C.

To build, the GNU Scientific Library (GSL) must be installed [libgsl0-dev].

For debugging and memory leak checking GDB and Valgrind are required.

For 'i2c_mpu6050', either the wiringPi library or i2c-dev library is required.

Refer to the README further in for usage details.



The 'Structured' program should be used.

Structure
---------
-> Ported 		- Algorithm ported into C directly from the provided MATLAB code.

-> Structured	- A restructured version of the 'Ported' program.

-> i2c_mpu6050 	- Example code using 2 different libraries to read a sensor 
				  directly on the RPI via I2C interface.