#ifndef _ITG3205_H_
#define _ITG3205_H_

#include <inttypes.h>
#include "../Wire/Wire.h"

#define ITG3205_ADDRESS 0x68

typedef struct GyroData{

   float X;
   float Y;
   float Z;
   float Temp;
}GyroData;


class ITG3205{
public:
   ITG3205();
   void calibrateGyro();
   void readGyro();

   GyroData getData();


private:
   static const int address = ITG3205_ADDRESS;
   float rawX, rawY, rawZ, rawTemp;

// Gyro offset, used in calibration
   int g_offx;
   int g_offy;
   int g_offz;
};

#endif
