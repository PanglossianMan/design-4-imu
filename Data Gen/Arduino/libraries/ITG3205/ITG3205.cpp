#include <Arduino.h>
#include "ITG3205.h"

ITG3205::ITG3205()
{
    Serial.println("Setting up ITG-3205 gyro...");

    g_offx = 0;
    g_offy = 0;
    g_offz = 0;

	Wire.beginTransmission(address);
	Wire.write(0x3E);
	Wire.write(0x00);
	Wire.endTransmission();

	Wire.beginTransmission(address);
	Wire.write(0x15);
	Wire.write(0x07);
	Wire.endTransmission();

	Wire.beginTransmission(address);
	Wire.write(0x16);
	Wire.write(0x1E);   // +/- 2000 dgrs/sec, 1KHz, 1E, 19
	Wire.endTransmission();

	Wire.beginTransmission(address);
	Wire.write(0x17);
	Wire.write(0x00);
	Wire.endTransmission();
}

void ITG3205::calibrateGyro(){
	int tmpx = 0;
	int tmpy = 0;
	int tmpz = 0;

	g_offx = 0;
	g_offy = 0;
	g_offz = 0;

	for (char i = 0;i<10;i++)
	{
		delay(10);
		readGyro();
		tmpx += rawX;
		tmpy += rawY;
		tmpz += rawZ;
	}
	g_offx = tmpx/10;
	g_offy = tmpy/10;
	g_offz = tmpz/10;
}

void ITG3205::readGyro(){
	Wire.beginTransmission(address);
	Wire.write(0x1B);
	Wire.endTransmission();

	Wire.beginTransmission(address);
	Wire.requestFrom(address, 8);    // request 8 bytes from ITG3200

	int i = 0;
	byte buff[8];
	while(Wire.available())
	{
		buff[i] = Wire.read();
		i++;
	}
	Wire.endTransmission();

	rawX = ((buff[4] << 8) | buff[5]) - g_offx;
	rawY = ((buff[2] << 8) | buff[3]) - g_offy;
	rawZ = ((buff[6] << 8) | buff[7]) - g_offz;
	rawTemp = (buff[0] << 8) | buff[1]; // temperature
}

GyroData ITG3205::getData(){
   GyroData data;
   data.X = rawX / 14.375;
   data.Y = rawY / 14.375;
   data.Z = rawZ / 14.375;
   data.Temp = 35+((rawTemp + 13200) / 280);

   return data;
}
