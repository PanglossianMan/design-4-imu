#include <Wire.h>
#include <string.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_ADXL345_U.h>
//#include <ITG3205.h>
#include <HMC5883L.h>

Adafruit_ADXL345_Unified g_accel;
//ITG3205 g_gyro;
HMC5883L g_comp;

// ACCEL VARS
struct AccelRawVals
{
  uint16_t x, y, z;
};

struct AccelScaledVals
{
  float x, y, z;
};

// GYRO VARS
struct GyroRawVals
{
  float x, y, z, temp;
};

struct GyroScaledVals
{
  float gyroDegX, gyroDegY, gyroDegZ, gyroActTemp;
};

/* ADXL345 is a 3-axis accelerometer, with output with 10-bits */
const float ACCEL_8G_SCALE = 15.625 / 1000 * 9.81; // from pg 4 of datasheet (mg/LSB)


// Accel vars
int16_t ax, ay, az;

unsigned long currentTime = 0;
unsigned long prevTime = 0;
unsigned long dt = 0;

char bufferStr[120];
char gBuffer[15];

struct AccelRawVals g_accelRawVals;
struct GyroRawVals g_gyroVals;
MagnetometerRaw g_compData;

void setup() {
  Serial.begin(115200);
  Wire.begin();
  Serial.println("Starting...");
  

  g_accel = Adafruit_ADXL345_Unified(12345);
  //g_gyro = ITG3205();
  g_comp = HMC5883L();

  if (!g_accel.begin())
  {
    Serial.println("ERROR: Unable to start accel");
    while(1){}
  }
  g_accel.setRange(ADXL345_RANGE_8_G);
  g_accel.setDataRate(ADXL345_DATARATE_200_HZ);
  g_accel.begin();
  //displaySensorDetails();

  //g_gyro.calibrateGyro();
  GyroCalibrate();

  // compass setup
  if (!g_comp.EnsureConnected())
  {
    Serial.println("ERROR: Unable to start comp");
    while(1){};
  }
  g_comp.SetScale(1.3);
  g_comp.SetMeasurementMode(Measurement_Continuous);
}

void loop() {
  prevTime = currentTime; 
  currentTime = micros();
  dt += currentTime - prevTime;

  // Read sensors every 10ms (100Hz)
  if (dt > 10000)
  { 
    // Read sensors
    g_accelRawVals.x = g_accel.getX();
    g_accelRawVals.y = g_accel.getY();
    g_accelRawVals.z = g_accel.getZ();
    GyroRead();
    g_compData = g_comp.ReadRawAxis();

    // scale accel values
    struct AccelScaledVals  accelScaled = 
    {
        g_accelRawVals.x * ACCEL_8G_SCALE,
        g_accelRawVals.y * ACCEL_8G_SCALE,
        g_accelRawVals.z * ACCEL_8G_SCALE
    };

    // Human readable but results in dt = 9 to 10ms
    //PrintDataString(accelScaled, g_gyroVals, g_compData);

    // Not human readable but results in dt = 1.73ms (consistent)
    PrintDataBinary(accelScaled, g_gyroVals, g_compData);

    Serial.println(dt);
    dt = 0;
  }
}
