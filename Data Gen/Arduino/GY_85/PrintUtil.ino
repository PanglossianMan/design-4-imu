

void PrintDataString(struct AccelRawVals &accelVals, struct GyroRawVals &gyroVals, MagnetometerRaw &magVals)
{
    Serial.print("Accel - ");
    Serial.print("X: "); Serial.print(accelVals.x); Serial.print("  ");
    Serial.print("Y: "); Serial.print(accelVals.y); Serial.print("  ");
    Serial.print("Z: "); Serial.print(accelVals.z); Serial.print("  ");
      
    Serial.print("Gyro - ");
    Serial.print("X: "); Serial.print(gyroVals.x); Serial.print("  ");
    Serial.print("Y: "); Serial.print(gyroVals.y); Serial.print("  ");
    Serial.print("Z: "); Serial.print(gyroVals.z); Serial.print("  ");
    
    Serial.print("Mag - ");
    Serial.print("X: "); Serial.print(magVals.XAxis); Serial.print("  ");
    Serial.print("Y: "); Serial.print(magVals.YAxis); Serial.print("  ");
    Serial.print("Z: "); Serial.print(magVals.ZAxis); Serial.println("");
}


void PrintDataBinary(struct AccelRawVals &accelVals, struct GyroRawVals &gyroVals, MagnetometerRaw &magVals)
{
    uint8_t dataLength = (uint8_t)(sizeof(struct AccelRawVals) + sizeof(struct GyroRawVals) + sizeof(MagnetometerRaw));
    size_t totalLength = 1 + dataLength + 1; // sizeof(data) | data | '\0'
    uint8_t data[totalLength];

    data[0] = dataLength;
    memcpy(&data[1], (void*)&accelVals, sizeof(struct AccelRawVals));
    memcpy(&data[1 + sizeof(struct AccelRawVals)], (void*)&gyroVals, sizeof(struct GyroRawVals));
    memcpy(&data[1 + sizeof(struct AccelRawVals) + sizeof(struct GyroRawVals)], (void*)&magVals, sizeof(MagnetometerRaw));
    data[totalLength-1] = (uint8_t) '\0';

    Serial.println((char*)data);
}

