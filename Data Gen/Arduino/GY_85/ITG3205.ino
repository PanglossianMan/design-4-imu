// This is to include the ITG3205 example to read the gyro
// as it seems to fail to work as a library at this time

// Variables declared before setup()
// float GyroRawX, GyroRawY, GyroRawZ, GyroRawTemp;
// float GyroDegX, GyroDegY, GyroDegZ, GyroActTemp;


int g_offx = 0;
int g_offy = 0;
int g_offz = 0;


#define ITG3200_Address 0x68

void initGyro() {
   // clear power management register
   // This ensures all gyros are not sleeping
   // and selects the Internal Oscillator as the clock source
   Wire.beginTransmission(ITG3200_Address); 
   Wire.write(0x3E);  
   Wire.write(0x01); // select X-axis gyro as clock source  
   Wire.endTransmission(); 
   
   // Set sample rate divider, sets the sample rate of the gyros
   // 0x07 sets the sample rate at 125Hz (8ms per sample)
   // if operating at 1kHz
   Wire.beginTransmission(ITG3200_Address); 
   Wire.write(0x15);  
   //Wire.write(0x07);   
   Wire.write(0x04); // 200Hz sample rate
   Wire.endTransmission(); 
   
   // Sets the DLPF register
   // configs several parameters related to sensor acquisition
   // FS_SEL (bits 3:4) - set to 3 for proper operation
   // DLPF_CFG (bits 2:0) - sets Low pass filter, this is being 
   // set to 5Hz
   Wire.beginTransmission(ITG3200_Address); 
   Wire.write(0x16);  
   Wire.write(0x1E);   // +/- 2000 dgrs/sec, 1KHz, 5Hz LPF, 1E, 19
   Wire.endTransmission(); 
  
   // Sets the intterupt config register
   // this disables interrupts from the sensor
   Wire.beginTransmission(ITG3200_Address); 
   Wire.write(0x17);  
   Wire.write(0x00);   
   Wire.endTransmission();   
}

void GyroCalibrate(){

 int tmpx = 0;
 int tmpy = 0;
 int tmpz = 0; 

 g_offx = 0;
 g_offy = 0;
 g_offz = 0;
 
 for (char i = 0;i<10;i++)
    {
    delay(10);  
    GyroRead();
    tmpx += g_gyroVals.x;
    tmpy += g_gyroVals.y;
    tmpz += g_gyroVals.z; 
    }  
 g_offx = tmpx/10;
 g_offy = tmpy/10;
 g_offz = tmpz/10;
}

// Reads the X Y Z values of the gyro and stores them in the variables declared
// at the start of this file
void GyroRead() {
  Wire.beginTransmission(ITG3200_Address); 
  Wire.write(0x1B);       
  Wire.endTransmission(); 
  
  Wire.beginTransmission(ITG3200_Address); 
  Wire.requestFrom(ITG3200_Address, 8);    // request 8 bytes from ITG3200
  
  int i = 0;
  byte buff[8];
  while(Wire.available())    
  { 
    buff[i] = Wire.read(); 
    i++;
  }
  Wire.endTransmission(); 

  
  g_gyroVals.x = ((buff[4] << 8) | buff[5]) - g_offx;
  g_gyroVals.y = ((buff[2] << 8) | buff[3]) - g_offy;
  g_gyroVals.z = ((buff[6] << 8) | buff[7]) - g_offz;
  g_gyroVals.temp = (buff[0] << 8) | buff[1]; // temperature  
  
  // Need to check conversion in data sheet
//  gyroDegX = g_gyroVals.x / 14.375;
//  gyroDegY = g_gyroVals.y / 14.375;
//  gyroDegZ = g_gyroVals.z / 14.375;
//  gyroActTemp = ( 35 + ( ( g_gyroVals.temp + 13200 ) / 280 ) );
}
