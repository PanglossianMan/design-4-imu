#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <assert.h>
#include <inttypes.h>
#include <string.h>
#include <stdbool.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>


void FPrintMatrix(const char *filename, const gsl_matrix *mat);
void PrintMatrix(const gsl_matrix *mat);
void PrintMatrixHex(const gsl_matrix *mat);

void FPrintVector(const char *filename, const gsl_vector *vector);
void PrintVector(const gsl_vector *vec);
void PrintVectorHex(const gsl_vector *vec);

void SetDiagonal(gsl_matrix *mat, const gsl_vector *vec);
bool CheckIfDiag(const gsl_matrix *mat);

typedef struct
{
	double sampleRate;
	int numSamples;
	gsl_matrix **Rotation;
	gsl_matrix **Concentration;
	gsl_matrix **R0;
	gsl_matrix **AngularVelocity;
	gsl_matrix **mgn;
	gsl_matrix **g;
	gsl_matrix *Heading;		// ?
	gsl_matrix *EulerAngles;	// ?
	gsl_matrix *Quaternion;		// ?
} result_t;

typedef struct
{
	gsl_vector *values;
	gsl_matrix *vectors;
} eigen_t;

void eigen_t_free(eigen_t *eigen);
void result_t_free(result_t *results);

#endif /* UTIL_H */
