#include "util.h"

/*
	Formats and prints the matrix to a text file

	Input:
		mat - matrix to save to file
*/
void
FPrintMatrix(const char *filename, const gsl_matrix *mat)
{
	assert(filename != NULL);
	assert(mat != NULL);

	FILE *fp;
	unsigned int row, column;

	fp = fopen(filename, "w");
	if (!fp)
	{
		fprintf(stderr, "Error: Unable to save matrix\n");
	}
	else
	{
		// TODO print formatted matrix to file
		// Get each row as a vector and print to the file

		for (row = 0; row < mat->size1; row++)
		{
			for (column = 0; column < mat->size2; column++)
			{
				fprintf(fp, "%8.6f\t", gsl_matrix_get(mat, row, column));
			}

			fprintf(fp, "\n");
		}

		fprintf(stdout, "Matrix saved [%s]\n", filename);
		fclose(fp);
	}


}


/*
	Prints a formatted matrix to stdout.
*/
void
PrintMatrix(const gsl_matrix *mat)
{
	assert(mat != NULL);

	for (unsigned int row = 0; row < mat->size1; row++)
		{
			for (unsigned int column = 0; column < mat->size2; column++)
			{
				fprintf(stdout, "%9.6f\t", gsl_matrix_get(mat, row, column));
			}

			fprintf(stdout, "\n");
		}
}

void
PrintMatrixHex(const gsl_matrix *mat)
{
assert(mat != NULL);

	for (unsigned int row = 0; row < mat->size1; row++)
	{
		for (unsigned int column = 0; column < mat->size2; column++)
		{
			uint64_t hexVal;
			double val = gsl_matrix_get(mat, row, column);
			memcpy(&hexVal, &val, sizeof(double));
			printf("%lX\t", hexVal);
			fprintf(stdout, "%g\t", gsl_matrix_get(mat, row, column));
		}

		fprintf(stdout, "\n");
	}
}



void
FPrintVector(const char *filename, const gsl_vector *vector)
{
	assert(filename != NULL);
	assert(vector != NULL);

	FILE *fp;
	unsigned int row;

	fp = fopen(filename, "w");
	if (!fp)
	{
		fprintf(stderr, "Error: Unable to save matrix\n");
	}
	else
	{
		// TODO print formatted matrix to file
		// Get each row as a vector and print to the file

		for (row = 0; row < vector->size; row++)
		{

			fprintf(fp, "%8.6f\n", gsl_vector_get(vector, row));
		}

		fprintf(stdout, "Vector saved [%s]\n", filename);
		fclose(fp);
	}


}

void PrintVector(const gsl_vector *vec)
{
	assert(vec != NULL);

	for (unsigned int row = 0; row < vec->size; row++)
	{
		fprintf(stdout, "%8.6f\n", gsl_vector_get(vec, row));
	}
}

void PrintVectorHex(const gsl_vector *vec)
{
	assert(vec != NULL);

	for (unsigned int row = 0; row < vec->size; row++)
	{
		uint64_t hexVal;
		double val = gsl_vector_get(vec, row);
		memcpy(&hexVal, &val, sizeof(double));
		printf("%lX\t", hexVal);
		fprintf(stdout, "%g\t", gsl_vector_get(vec, row));
	}
}

void
SetDiagonal(gsl_matrix *mat, const gsl_vector *vec)
{
    assert(mat != NULL);
    assert(vec != NULL);
    
	for (unsigned int d = 0; d < mat->size1 && d < mat->size2 && d < vec->size; d++)
	{
		gsl_matrix_set(mat, d, d, gsl_vector_get(vec, d));
	}
}


bool
CheckIfDiag(const gsl_matrix *mat)
{
    assert(mat != NULL);

	double val;
	for (unsigned int i = 0; i < mat->size1; i++)
	{
		for (unsigned int j = 0; j < mat->size2; j++)
		{
			if (i != j && (val = gsl_matrix_get(mat, i, j)) != 0.0)
			{
				fprintf(stdout, "[%d, %d] is not 0 (%f). Matrix is not diagonal\n",
					i, j, val);
				return false;
			}
		}
	}
    return true;
}



void eigen_t_free(eigen_t *eigen)
{
    assert(eigen != NULL);

	gsl_vector_free(eigen->values);
    eigen->values = NULL;
	gsl_matrix_free(eigen->vectors);
    eigen->vectors = NULL;
}

void result_t_free(result_t *results)
{
    assert(results != NULL);

	if (results->numSamples > 0)
	{
		for (int i = 0; i < results->numSamples; i++)
		{
			gsl_matrix_free(results->Rotation[i]);
			gsl_matrix_free(results->Concentration[i]);
			gsl_matrix_free(results->R0[i]);
			gsl_matrix_free(results->AngularVelocity[i]);
			gsl_matrix_free(results->mgn[i]);
			gsl_matrix_free(results->g[i]);
		}
		free(results->Rotation);
		results->Rotation = NULL;
		free(results->Concentration);
		results->Concentration = NULL;
		free(results->R0);
		results->R0 = NULL;
		free(results->AngularVelocity);
		results->AngularVelocity = NULL;
		free(results->mgn);
		results->mgn = NULL;
		free(results->g);
		results->g = NULL;

		gsl_matrix_free(results->Heading);
		results->Heading = NULL;
		gsl_matrix_free(results->EulerAngles);
		results->EulerAngles = NULL;
		gsl_matrix_free(results->Quaternion);
		results->Quaternion = NULL;

		results->sampleRate = 0.0;
		results->numSamples = 0;
	}
}
