#include "imu_algorithm.h"

#include <math.h>
#include <stdlib.h>
#include <stdbool.h>


result_t
ApplyAlgorithm(gsl_matrix *inputMatrix)
{
	double sampleFreq = SAMPLE_FREQ;

	gsl_matrix_view accelView, gyroView, magView;
	accelView = gsl_matrix_submatrix(inputMatrix, 0, 1, inputMatrix->size1, 3);
	gyroView = gsl_matrix_submatrix(inputMatrix, 0, 4, inputMatrix->size1, 3);
	magView = gsl_matrix_submatrix(inputMatrix, 0, 10, inputMatrix->size1, 3);

	gsl_matrix *accMatrix, *gyroMatrix, *mgnMatrix;
	accMatrix = gsl_matrix_alloc((&accelView.matrix)->size2, (&accelView.matrix)->size1);
	gyroMatrix = gsl_matrix_alloc((&gyroView.matrix)->size2, (&gyroView.matrix)->size1);
	mgnMatrix = gsl_matrix_alloc((&magView.matrix)->size2, (&magView.matrix)->size1);

	gsl_matrix_transpose_memcpy(accMatrix, &accelView.matrix);
	gsl_matrix_transpose_memcpy(gyroMatrix, &gyroView.matrix);
	gsl_matrix_transpose_memcpy(mgnMatrix, &magView.matrix);

	int numSamples = inputMatrix->size1;

	// gravity acceleration
	gsl_matrix *gravityMatrix = gsl_matrix_alloc(3, 1);
	gsl_matrix_set_zero(gravityMatrix);
	gsl_matrix_set(gravityMatrix, 2, 0, 1);

	// magnetic field for Melbourne
	// h = [21313.3;
	//		4388.4;
	//		55947.2]
	gsl_matrix *magFieldMatrix = gsl_matrix_alloc(3, 1);
	gsl_matrix_set(magFieldMatrix, 0, 0, 21313.3);
	gsl_matrix_set(magFieldMatrix, 1, 0, 4388.4);
	gsl_matrix_set(magFieldMatrix, 2, 0, 55947.2);

	// Normalise magnetic field (matrix h0)
	gsl_vector_view magFieldVectorView = gsl_matrix_column(magFieldMatrix, 0);
	double magFieldNorm = gsl_blas_dnrm2(&magFieldVectorView.vector);
	gsl_matrix_scale(magFieldMatrix, 1/magFieldNorm);


	double dArray[] = {1000.0f, 10.0f};
	gsl_vector_view dVectorView = gsl_vector_view_array(dArray, 2);
	gsl_matrix *d = gsl_matrix_calloc(2, 2);
	SetDiagonal(d, &dVectorView.vector);

	/*
		Normalized Y-axis accel and mag values
	*/
	// Find norm of Y-axis accel values
	gsl_vector_view accelYAxisVectorView = gsl_matrix_column(accMatrix, 0);
	double accelYAxisNorm = gsl_blas_dnrm2(&accelYAxisVectorView.vector);
	// 0.9623
	// Find norm of Y-axis mag values
	gsl_vector_view magYAxisVectorView = gsl_matrix_column(mgnMatrix, 0);
	double magYaxisNorm = gsl_blas_dnrm2(&magYAxisVectorView.vector);

	// size of matrix is number of axis (rows), by accel and mag readings (columns)
	gsl_matrix *y = gsl_matrix_alloc(3, 2);
	// copy accel and mag values into y matrix
	gsl_matrix_set_col(y, 0, &accelYAxisVectorView.vector);
	gsl_matrix_set_col(y, 1, &magYAxisVectorView.vector);
	// create mat view of accel and mag values, change view from input matrix to
	// y matrix
	accelYAxisVectorView = gsl_matrix_column(y, 0);
	magYAxisVectorView = gsl_matrix_column(y, 1);
	// scale accel and mag values by their norm
	gsl_vector_scale(&accelYAxisVectorView.vector, 1/accelYAxisNorm);
	gsl_vector_scale(&magYAxisVectorView.vector, 1/magYaxisNorm);


	/*
		X matrix
	*/
	gsl_matrix *x = gsl_matrix_alloc(3, 2);
	gsl_vector_view gravityVectorView = gsl_matrix_column(gravityMatrix, 0);
	double gravityNorm = gsl_blas_dnrm2(&gravityVectorView.vector);
	gsl_vector_view magFieldVectorView2 = gsl_matrix_column(magFieldMatrix, 0);
	double magFieldNorm2 = gsl_blas_dnrm2(&magFieldVectorView2.vector);
	gsl_matrix_set_col(x, 0, &gravityVectorView.vector);
	gsl_matrix_set_col(x, 1, &magFieldVectorView2.vector);
	gravityVectorView = gsl_matrix_column(x, 0);
	magFieldVectorView2 = gsl_matrix_column(x, 1);
	gsl_vector_scale(&gravityVectorView.vector, 1/gravityNorm);
	gsl_vector_scale(&magFieldVectorView2.vector, 1/magFieldNorm2);

	/*
		[RotationS03.m:51] polar_left(X*D*Y')
		Product of X, D, and transpose of Y.
	*/
	// X*D*Y'
	// Do X*D with temp X matrix
	gsl_matrix *xd = gsl_matrix_calloc(x->size1, d->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					1.0, x, d,
					0.0, xd);
	// Do (X*D) * Y'
	// create array for 0's
	gsl_matrix *out = gsl_matrix_calloc(xd->size1, y->size1);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					1.0, xd, y,
					0.0, out);

	gsl_matrix **ra = polar_left(out);


	// Qd
	gsl_matrix *qd = gsl_matrix_calloc(6, 6);

	gsl_matrix_set_identity(qd);
	gsl_matrix_view qdView = gsl_matrix_submatrix(qd, 0, 0, 3, 3);
	gsl_matrix_scale(&qdView.matrix, 1E-4);
	qdView = gsl_matrix_submatrix(qd, 3, 3, 3, 3);
	gsl_matrix_scale(&qdView.matrix, 1E-2);


	// Qm
	gsl_matrix *qm = gsl_matrix_calloc(3, 3);
	gsl_matrix_set_identity(qm);

	// P
	gsl_matrix *p = gsl_matrix_calloc(6, 6);
	gsl_matrix_set_identity(p);

	// H
	gsl_matrix *h = gsl_matrix_calloc(3, 6);
	gsl_matrix_set_identity(h);

	// State
	gsl_matrix *state = gsl_matrix_calloc(6, 1);

	for (int i = 0; i < 3; i++)
	{
		gsl_matrix_set(state, i, 0, -gsl_matrix_get(gyroMatrix, i, 0));
	}

	// F
	gsl_matrix *f = gsl_matrix_calloc(6, 6);
	gsl_matrix_set_identity(f);

	gsl_matrix_view fView = gsl_matrix_submatrix(f, 0, 3, 3, 3);
	gsl_matrix_set_identity(&fView.matrix);
	gsl_matrix_scale(&fView.matrix, 1/sampleFreq);


	/*
		Initialise result_t struct and allocate memory.
	*/

	// zero struct
	// results = {0}; generates a warning from gcc
	result_t results;
	memset(&results, 0, sizeof(result_t));

	results.sampleRate = sampleFreq;
	results.numSamples = numSamples;
	results.Rotation = (gsl_matrix**) calloc(numSamples, sizeof(gsl_matrix*));
	results.Concentration = (gsl_matrix**) calloc(numSamples, sizeof(gsl_matrix*));
	results.R0 = (gsl_matrix**) calloc(numSamples, sizeof(gsl_matrix*));
	results.AngularVelocity = (gsl_matrix**) calloc(numSamples, sizeof(gsl_matrix*));
	results.mgn = (gsl_matrix**) calloc(numSamples, sizeof(gsl_matrix*));
	results.g = (gsl_matrix**) calloc(numSamples, sizeof(gsl_matrix*));
	results.EulerAngles = gsl_matrix_calloc(3, numSamples);

	for (int i = 0; i < numSamples; i++)
	{
		results.Rotation[i] = gsl_matrix_calloc(3, 3);
		results.Concentration[i] = gsl_matrix_calloc(3, 3);
		results.R0[i] = gsl_matrix_calloc(3, 3);
		results.AngularVelocity[i] = gsl_matrix_calloc(gyroMatrix->size1, 1);
		results.mgn[i] = gsl_matrix_calloc(ra[0]->size2, 1);					// res.magn(:,1)=R'*mgn(:,1);
		results.g[i] = gsl_matrix_calloc(ra[0]->size2, 1);						// res.g(:,1)=R'*acc(:,1);
	}


	// Load first calculated results into results struct
	gsl_matrix_memcpy(results.Rotation[0], ra[0]);
	gsl_matrix_memcpy(results.Concentration[0], ra[1]);
	gsl_matrix_memcpy(results.R0[0], ra[0]);

	// copy first gyro readings into first column of
	// matrix results.AngularVelocity[0]
	double gyroVal;
	for (int i = 0; i < 3; i++)
	{
		gyroVal = gsl_matrix_get(gyroMatrix, i, 0);
		gsl_matrix_set(results.AngularVelocity[0], i, 0, gyroVal);
	}

	// res.mgn(:,1) = R' * mgn(:,1);
	gsl_matrix_view mgnView = gsl_matrix_submatrix(mgnMatrix, 0, 0, mgnMatrix->size1, 1);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans,
					1.0, ra[0], &mgnView.matrix,
					0.0, results.mgn[0]);

	// res.g(:,1) = R' * acc(:,1);
	gsl_matrix_view accView = gsl_matrix_submatrix(accMatrix, 0, 0, accMatrix->size1, 1);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans,
					1.0, ra[0], &accView.matrix,
					0.0, results.g[0]);

	/*
		Matrix declarations for loop
	*/
	double tempMgnArray[3] = {0};
	gsl_matrix_view tempMgnView = gsl_matrix_view_array(tempMgnArray, 3, 1);
	double tempAccArray[3] = {0};
	gsl_matrix_view tempAccView = gsl_matrix_view_array(tempAccArray, 3, 1);
	double tempMgnResArray[3] = {0};
	gsl_matrix_view tempMgnResView = gsl_matrix_view_array(tempMgnResArray, 3, 1);
	double tempAccResArray[3] = {0};
	gsl_matrix_view tempAccResView = gsl_matrix_view_array(tempAccResArray, 3, 1);


	gsl_matrix *statep = gsl_matrix_alloc(f->size1, state->size2); // [6x1]

	gsl_matrix *pp = gsl_matrix_alloc(6, 6);
	gsl_matrix *tempFP = gsl_matrix_alloc(6, 6);
	gsl_matrix *tempFPFT = gsl_matrix_alloc(6, 6);

	gsl_matrix *s = gsl_matrix_alloc(3, 3);
	gsl_matrix *tempHPP = gsl_matrix_alloc(3, 6);
	gsl_matrix *tempHPPHT = gsl_matrix_alloc(3, 3);

	gsl_matrix *k = gsl_matrix_alloc(6, 3);
	gsl_matrix *tempPPHT = gsl_matrix_alloc(6, 3);
	gsl_matrix *tempInvS = gsl_matrix_alloc(3, 3);

	gsl_matrix *w = gsl_matrix_alloc(3, 1);
	gsl_matrix *tempHStatep = gsl_matrix_alloc(3, 1);

	// gsl_matrix *tempKW = gsl_matrix_alloc(6, 1);

	gsl_matrix *tempEyeKH = gsl_matrix_alloc(6, 6);

	gsl_matrix *omega = NULL;

	gsl_matrix *w0 = gsl_matrix_alloc(3, 3);

	gsl_matrix *a0 = gsl_matrix_alloc(ra[1]->size1, ra[1]->size2);

	gsl_matrix *tempB0W0 = gsl_matrix_alloc(3, 3);
	gsl_matrix *tempB0W0Sigma = gsl_matrix_alloc(3, 3);
	gsl_matrix *tempB0W0SigmaR = gsl_matrix_alloc(3, 3);
	gsl_matrix **coninv0Result = NULL;


    /*
     *
     * Start of loop
     *
     */

	for (int i = 1; i < numSamples; i++)
	{

		// Y = [acc(:,i) / norm(acc(:,i))   mgn(:,i) / norm(mgn(:,i))];
		gsl_vector_view accColumnView = gsl_matrix_column(accMatrix, i);
		gsl_vector_view mgnColumnView = gsl_matrix_column(mgnMatrix, i);

		for (int i = 0; i < 3; i++)
		{
			gsl_matrix_set(&tempAccView.matrix, i, 0,
				gsl_vector_get(&accColumnView.vector, i));
			gsl_matrix_set(&tempMgnView.matrix, i, 0,
				gsl_vector_get(&mgnColumnView.vector, i));
		}

		double accNorm = gsl_blas_dnrm2(&accColumnView.vector);
		gsl_vector_scale(&accColumnView.vector, 1/accNorm);

		double mgnNorm = gsl_blas_dnrm2(&mgnColumnView.vector);
		gsl_vector_scale(&mgnColumnView.vector, 1/mgnNorm);

		gsl_matrix_set_col(y, 0, &accColumnView.vector);
		gsl_matrix_set_col(y, 1, &mgnColumnView.vector);

		// [R0] = polar_left(X * D * Y');
		// X*D*Y'
		// Do X*D with temp X matrix
		gsl_matrix_set_zero(xd);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, x, d,
						0.0, xd);
		// Do (X*D) * Y'
		// create array for 0's
		gsl_matrix_set_zero(out);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
						1.0, xd, y,
						0.0, out);
		gsl_matrix **r0 = polar_left(out);

		gsl_matrix_memcpy(results.R0[i], r0[0]);



		// statep = F * state;
		gsl_matrix_set_zero(statep);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, f, state,
						0.0, statep);


		// Pp = F * P * F' + Qd;
		// (F * P)
		gsl_matrix_set_zero(tempFP);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, f, p,
						0.0, tempFP);
		// * F'
		gsl_matrix_set_zero(tempFPFT);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
						1.0, tempFP, f,
						0.0, tempFPFT);
		// + Qd
		gsl_matrix_memcpy(pp, tempFPFT);
		gsl_matrix_add(pp, qd);



		// S = H * Pp * H' + Qm;
		// S is the 3x3 top left submatrix of Pp + Qm
		// H * Pp
		gsl_matrix_set_zero(tempHPP);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, h, pp,
						0.0, tempHPP);
		// * H'
		gsl_matrix_set_zero(tempHPPHT);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
						1.0, tempHPP, h,
						0.0, tempHPPHT);
		// + Qm
		gsl_matrix_memcpy(s, tempHPPHT);
		gsl_matrix_add(s, qm);


		// K = Pp * H' / S
		//   = Pp * H' * S^-1;
		// If S is always diagonal, The values in S^-1 are the inverse of
		// those in S
		// NOTE: S is assumed to be a diagonal matrix
		if (CheckIfDiag(s))
		{
			// Pp * H'
			gsl_matrix_set_zero(tempPPHT);
			gsl_blas_dgemm(CblasNoTrans, CblasTrans,
							1.0, pp, h,
							0.0, tempPPHT);
			// create S^-1 from S
			gsl_matrix_memcpy(tempInvS, s);
			gsl_matrix_set(tempInvS, 0, 0, 1 / gsl_matrix_get(s, 0, 0));
			gsl_matrix_set(tempInvS, 1, 1, 1 / gsl_matrix_get(s, 1, 1));
			gsl_matrix_set(tempInvS, 2, 2, 1 / gsl_matrix_get(s, 2, 2));

			// (Pp * H') * S^-1
			gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
							1.0, tempPPHT, tempInvS,
							0.0, k);
		}
		else
		{
			fprintf(stderr, "[loop count = %d] S is not diagonal, exiting...\n", i);
			exit(-1);
		}

		// w = -gyro(:,i) - H * statep;
		//
		// -gyro(:,i)
		gyroView = gsl_matrix_submatrix(gyroMatrix, 0, i, gyroMatrix->size1, 1);
		gsl_matrix_memcpy(w, &gyroView.matrix);
		gsl_matrix_scale(w, -1.0);
		// H * Statep
		gsl_matrix_set_zero(tempHStatep);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, h, statep,
						0.0, tempHStatep);
		// -gyro(:,i) - H * Statep
		gsl_matrix_sub(w, tempHStatep);
		// state = statep + K * w;
		// K * w
		gsl_matrix_set_zero(state);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, k, w,
						0.0, state);
		// + statep
		gsl_matrix_add(state, statep);


		// P = (eye(6) - K * H) * Pp;
		// eye(6) - K * H
		gsl_matrix_set_identity(tempEyeKH);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						-1.0, k, h,
						1.0, tempEyeKH);
		// * Pp
		gsl_matrix_set_zero(p);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, tempEyeKH, pp,
						0.0, p);



		// Omega = tensorop(state(1:3));
		gsl_vector_view tempVectView = gsl_matrix_subcolumn(state, 0, 0, 3);
		omega = tensorop(&tempVectView.vector);



		// W0 = expm(Omega / f);
		//
		// NOTE: This function is officially unsupported (as far as I can tell).
		// and is hence not in the documentation. See the following link.
		// https://lists.gnu.org/archive/html/help-gsl/2009-12/msg00032.html
		gsl_matrix_scale(omega, 1/sampleFreq);
		gsl_linalg_exponential_ss(omega, w0, 0.01);



		// B0 = baker_cov(P(1:3,1:3) / f^2, state(1:3) / f);
		// The following creates temporary copies of P and state, then appropriately
		// multiplies (divides) them before applying baker_cov.
		// P(1:3, 1:3) / f^2
		double pTempMat[36] = {0};
		gsl_matrix_view pTempMatView = gsl_matrix_view_array(pTempMat, 6, 6);
		gsl_matrix_memcpy(&pTempMatView.matrix, p);
		gsl_matrix_view pTempSubMat = gsl_matrix_submatrix(&pTempMatView.matrix, 0, 0, 3, 3);
		gsl_matrix_scale(&pTempSubMat.matrix, 1/(sampleFreq * sampleFreq));
		// state(1:3) / f
		double stateTempMat[6] = {0};
		gsl_matrix_view stateTempMatView = gsl_matrix_view_array(stateTempMat, 6, 1);
		gsl_matrix_memcpy(&stateTempMatView.matrix, state);
		gsl_matrix_view stateTempSubMat = gsl_matrix_submatrix(&stateTempMatView.matrix, 0, 0, 3, 1);
		gsl_matrix_scale(&stateTempSubMat.matrix, 1/sampleFreq);
		// baker_cov()
		gsl_matrix **bs = baker_cov(&pTempSubMat.matrix, &stateTempSubMat.matrix);



		// A0 = A
		gsl_matrix_memcpy(a0, ra[1]);



		// [R,A]=coninv0(B0 * W0 * SigmaHat0(A0) * R);
		// create temp R
		double tempRArray[9] = {0};
		gsl_matrix_view tempRView = gsl_matrix_view_array(tempRArray, 3, 3);
		gsl_matrix_memcpy(&tempRView.matrix, ra[0]);
		// free ra
		gsl_matrix_free(ra[0]);
		gsl_matrix_free(ra[1]);
		//free(ra);
		// B0 * W0
		gsl_matrix_set_zero(tempB0W0);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, bs[0], w0,
						0.0, tempB0W0);
		// * Sigmahat0(A0)
		gsl_matrix *sighat = SigmaHat0(a0);
		gsl_matrix_set_zero(tempB0W0Sigma);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, tempB0W0, sighat,
						0.0, tempB0W0Sigma);
		// * R
		gsl_matrix_set_zero(tempB0W0SigmaR);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, tempB0W0Sigma, &tempRView.matrix,
						0.0, tempB0W0SigmaR);


		// [R,A] = coninv0()
		coninv0Result = coninv0(tempB0W0SigmaR);
		ra[0] = coninv0Result[0];
		ra[1] = coninv0Result[1];
		coninv0Result[0] = NULL;
		coninv0Result[1] = NULL;


		// [R,A] = polar_left(X * D * Y' + R' * A);
		// X*D*Y'
		// Do X*D with temp X matrix
		gsl_matrix_set_zero(xd);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, x, d,
						0.0, xd);
		// Do (X*D) * Y'
		// create array for 0's
		gsl_matrix_set_zero(out);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
						1.0, xd, y,
						0.0, out);

		// R' * A
		double tempRAArray[9] = {0};
		gsl_matrix_view tempRAView = gsl_matrix_view_array(tempRAArray, 3, 3);
		gsl_blas_dgemm(CblasTrans, CblasNoTrans,
						1.0, ra[0], ra[1],
						0.0, &tempRAView.matrix);
		// X * D * Y' + R' * A
		gsl_matrix_add(out, &tempRAView.matrix);
		// [R, A] = polar_left()
		gsl_matrix** polarLeftResult = polar_left(out);

		gsl_matrix_memcpy(ra[0], polarLeftResult[0]);
		gsl_matrix_memcpy(ra[1], polarLeftResult[1]);


		/*
			Record results
		*/
		gsl_matrix_memcpy(results.Rotation[i], polarLeftResult[0]);
		gsl_matrix_memcpy(results.Concentration[i], polarLeftResult[1]);

		gsl_matrix_view stateSubMat = gsl_matrix_submatrix(state, 0, 0, 3, 1);
		gsl_matrix_memcpy(results.AngularVelocity[i], &stateSubMat.matrix);

		// R' * mgn(:, i)
		gsl_blas_dgemm(CblasTrans, CblasNoTrans,
						1.0, polarLeftResult[0], &tempMgnView.matrix,
						0.0, &tempMgnResView.matrix);
		// R' * acc(:, i);
		gsl_blas_dgemm(CblasTrans, CblasNoTrans,
						1.0, polarLeftResult[0], &tempAccView.matrix,
						0.0, &tempAccResView.matrix);
		gsl_matrix_memcpy(results.mgn[i], &tempMgnResView.matrix);
		gsl_matrix_memcpy(results.g[i], &tempAccResView.matrix);


		/*
			Clean up
		*/
		gsl_matrix_free(polarLeftResult[1]);
		gsl_matrix_free(polarLeftResult[0]);
		free(polarLeftResult);
		if (coninv0Result[0] != NULL)
		{
			gsl_matrix_free(coninv0Result[0]);
			coninv0Result[0] = NULL;
		}
		if (coninv0Result[1] != NULL)
		{
			gsl_matrix_free(coninv0Result[1]);
			coninv0Result[1] = NULL;
		}
		//gsl_matrix_free(ra[0]);
		//gsl_matrix_free(ra[1]);
		gsl_matrix_free(coninv0Result[2]);
		coninv0Result[2] = NULL;
		free(coninv0Result);
		coninv0Result = NULL;
		gsl_matrix_free(sighat);
		sighat = NULL;
		gsl_matrix_free(bs[0]);
		gsl_matrix_free(bs[1]);
		free(bs);
		bs = NULL;
		gsl_matrix_free(omega);
		omega = NULL;
		gsl_matrix_free(r0[0]);
		gsl_matrix_free(r0[1]);
		free(r0);
		r0 = NULL;
	}


	// Populate results.EulerAngles
	for (int i = 0; i < results.numSamples; i++)
	{
		gsl_matrix* angles = DCM2Euler(results.Rotation[i]);
		gsl_matrix_view angleSubMat = gsl_matrix_submatrix(results.EulerAngles, 0, i, 3, 1);
		gsl_matrix_memcpy(&angleSubMat.matrix, angles);
		gsl_matrix_free(angles);
	}

	/*
		Clean up
	*/
	if (coninv0Result != NULL)
	{
		if (coninv0Result[2] != NULL) gsl_matrix_free(coninv0Result[2]);
		if (coninv0Result[1] != NULL) gsl_matrix_free(coninv0Result[1]);
		if (coninv0Result[0] != NULL) gsl_matrix_free(coninv0Result[0]);
		free(coninv0Result);
	}
	gsl_matrix_free(tempB0W0SigmaR);
	gsl_matrix_free(tempB0W0Sigma);
	gsl_matrix_free(tempB0W0);
	gsl_matrix_free(a0);
	gsl_matrix_free(w0);
	gsl_matrix_free(omega);
	gsl_matrix_free(tempEyeKH);
	gsl_matrix_free(tempHStatep);
	gsl_matrix_free(w);
	gsl_matrix_free(tempInvS);
	gsl_matrix_free(tempPPHT);
	gsl_matrix_free(k);
	gsl_matrix_free(tempHPPHT);
	gsl_matrix_free(tempHPP);
	gsl_matrix_free(s);
	gsl_matrix_free(tempFPFT);
	gsl_matrix_free(tempFP);
	gsl_matrix_free(pp);
	gsl_matrix_free(statep);
	gsl_matrix_free(f);
	gsl_matrix_free(state);
	gsl_matrix_free(h);
	gsl_matrix_free(p);
	gsl_matrix_free(qm);
	gsl_matrix_free(qd);
	gsl_matrix_free(ra[0]);
	gsl_matrix_free(ra[1]);
	free(ra);
	gsl_matrix_free(out);
	gsl_matrix_free(xd);
	gsl_matrix_free(x);
	gsl_matrix_free(y);
	gsl_matrix_free(d);
	gsl_matrix_free(magFieldMatrix);
	gsl_matrix_free(gravityMatrix);
	gsl_matrix_free(mgnMatrix);
	gsl_matrix_free(gyroMatrix);
	gsl_matrix_free(accMatrix);

	return results;
}
