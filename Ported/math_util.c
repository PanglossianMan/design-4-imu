#include "math_util.h"

gsl_matrix** polar_left(const gsl_matrix *mat)
{
    assert(mat != NULL);

	// mat will be 3x3
	gsl_matrix **ra;
	ra = (gsl_matrix**) malloc(2 * sizeof(gsl_matrix*));
	if (ra == NULL)
	{
		fprintf(stderr, "Error malloc <polar_left>\n");
		exit(0);
	}

	ra[0] = NULL;
	ra[1] = NULL;


	// allocate U, S & V matricies and vectors
	gsl_matrix *u = gsl_matrix_calloc(mat->size1, mat->size2);
	gsl_matrix *v = gsl_matrix_calloc(mat->size2, mat->size2);
	gsl_vector *s = gsl_vector_calloc(mat->size1);
	gsl_vector *workspace = gsl_vector_calloc(mat->size2);

	gsl_matrix_memcpy(u, mat);

	// calc SVD(mat)
	// mat is replaced with the resulting matrix U.
	//
	// The diagonal elements in S are stored in the vector s.
	gsl_linalg_SV_decomp(
		u,
		v,
		s,
		workspace);


	gsl_matrix_view vSubMat = gsl_matrix_submatrix(v, 0, 2, 3, 1);
	gsl_matrix_scale(&vSubMat.matrix, -1.0f);

	// allocate and calculate Q = V * U'
	gsl_matrix *q = gsl_matrix_calloc(v->size1, u->size1);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					1.0, v, u,
					0.0, q);

	// allocate and calculate A = V * S * V'
	gsl_matrix *sDiagMatrix = gsl_matrix_calloc(s->size, s->size);
	SetDiagonal(sDiagMatrix, s);

	gsl_matrix *vs = gsl_matrix_calloc(v->size1, sDiagMatrix->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					1.0, v, sDiagMatrix,
					0.0, vs);

	ra[1] = gsl_matrix_calloc(vs->size1, v->size1);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					1.0, vs, v,
					0.0, ra[1]);


	// if abs(det(Q) + 1) < 0.01
	double detQ = get_det(q);
	if (fabs(detQ + 1.0f) < 0.01)
	{
		// 	R = V * diag([ones(1, 2) -1]) * U'
		//
		// 	ones(1, 2) 	- creates a 1 by 2 matrix of all ones
		// 				ones(1, 2) = 	[1 1]
		//
		//	diag(v) 	- creates a matrix with the values of v in the diagonal
		// 				diag([1 2]) = 	[1 0]
		//								[0 2]

		// Create diagonal matrix
		gsl_vector *vect = gsl_vector_calloc(3);
		gsl_vector_set_all(vect, 1.0f);
		gsl_vector_set(vect, 2, -1.0f);

		gsl_matrix *diagMat = gsl_matrix_calloc(3, 3);
		SetDiagonal(diagMat, vect);

		gsl_matrix *vDiag = gsl_matrix_calloc(v->size1, diagMat->size2);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, v, diagMat,
						0.0, vDiag);


		ra[0] = gsl_matrix_calloc(vDiag->size1, u->size1);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
						1.0, vDiag, u,
						0.0, ra[0]);

		gsl_matrix_free(vDiag);
		gsl_matrix_free(diagMat);
		gsl_vector_free(vect);

	}
	else if (fabs(detQ - 1.0f) < 0.01)
	{
		// R = Q
		ra[0] = gsl_matrix_calloc(q->size1, q->size2);
		gsl_matrix_memcpy(ra[0], q);
	}
	else
	{
		fprintf(stderr, "Error in polar_left\n");
		gsl_matrix_free(ra[1]);
		ra[1] = NULL;
		return ra;
	}

	/*
		Clean up
	*/
	gsl_matrix_free(vs);
	gsl_matrix_free(sDiagMatrix);
	gsl_matrix_free(q);
	gsl_vector_free(workspace);
	gsl_matrix_free(u);
	gsl_vector_free(s);
	gsl_matrix_free(v);

	return ra;
}


gsl_matrix** polar_right(const gsl_matrix *mat)
{
    assert(mat != NULL);

	// mat will be 3x3
	gsl_matrix **ra;
	ra = (gsl_matrix**) malloc(2 * sizeof(gsl_matrix*));
	if (ra == NULL)
	{
		fprintf(stderr, "Error malloc <polar_left>\n");
		exit(0);
	}

	ra[0] = NULL;
	ra[1] = NULL;


	// allocate U, S & V matricies and vectors
	gsl_matrix *u = gsl_matrix_calloc(mat->size1, mat->size2);
	gsl_matrix *v = gsl_matrix_calloc(mat->size2, mat->size2);
	gsl_vector *s = gsl_vector_calloc(mat->size1);
	gsl_vector *workspace = gsl_vector_calloc(mat->size2);

	gsl_matrix_memcpy(u, mat);

	// calc SVD(mat)
	// mat is replaced with the resulting matrix U.
	//
	// The diagonal elements in S are stored in the vector s.
	gsl_linalg_SV_decomp(
		u,
		v,
		s,
		workspace);


	// allocate and calculate Q = U * V'
	gsl_matrix *q = gsl_matrix_calloc(u->size1, v->size1);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					1.0, u, v,
					0.0, q);


	// if abs(det(Q) - 1) < 0.01
	double detQ = get_det(q);
	if (fabs(detQ - 1.0f) < 0.01)
	{
		// R = Q
		ra[0] = gsl_matrix_calloc(q->size1, q->size2);
		gsl_matrix_memcpy(ra[0], q);
	}
	else if (fabs(detQ + 1.0f) < 0.01)
	{
		// 	R = V * diag([ones(1, 2) -1]) * U'
		//
		// 	ones(1, 2) 	- creates a 1 by 2 matrix of all ones
		// 				ones(1, 2) = 	[1 1]
		//
		//	diag(v) 	- creates a matrix with the values of v in the diagonal
		// 				diag([1 2]) = 	[1 0]
		//								[0 2]

		// Create diagonal matrix
		gsl_vector *vect = gsl_vector_calloc(3);
		gsl_vector_set_all(vect, 1.0f);
		gsl_vector_set(vect, 2, -1.0f);

		gsl_matrix *diagMat = gsl_matrix_calloc(3, 3);
		SetDiagonal(diagMat, vect);

		gsl_matrix *vDiag = gsl_matrix_calloc(v->size1, diagMat->size2);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						1.0, v, diagMat,
						0.0, vDiag);


		ra[0] = gsl_matrix_calloc(vDiag->size1, u->size1);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
						1.0, vDiag, u,
						0.0, ra[0]);

		gsl_matrix_free(vDiag);
		gsl_matrix_free(diagMat);
		gsl_vector_free(vect);
	}
	else
	{
		fprintf(stderr, "Error in polar_right\n");
		return ra;
	}


	// allocate and calculate A = U * S * U'
	gsl_matrix *sDiagMatrix = gsl_matrix_calloc(s->size, s->size);
	SetDiagonal(sDiagMatrix, s);

	gsl_matrix *us = gsl_matrix_calloc(u->size1, sDiagMatrix->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					1.0, u, sDiagMatrix,
					0.0, us);

	ra[1] = gsl_matrix_calloc(us->size1, u->size1);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					1.0, us, u,
					0.0, ra[1]);


	/*
		Clean up
	*/
	gsl_matrix_free(us);
	gsl_matrix_free(sDiagMatrix);
	gsl_matrix_free(q);
	gsl_vector_free(workspace);
	gsl_matrix_free(u);
	gsl_vector_free(s);
	gsl_matrix_free(v);

	return ra;
}


gsl_matrix* tensorop(const gsl_vector *vect)
{
    assert(vect != NULL);
	/*
		w =
		[   0     -vect[2]   vect[1]]
		[vect[2]     0      -vect[0]]
		[-vect[1]  vect[0]     0    ]
	*/

	gsl_matrix *w = gsl_matrix_calloc(3, 3);
	gsl_matrix_set(w, 0, 1, -gsl_vector_get(vect, 2));
	gsl_matrix_set(w, 0, 2, gsl_vector_get(vect, 1));

	gsl_matrix_set(w, 1, 0, gsl_vector_get(vect, 2));
	gsl_matrix_set(w, 1, 2, -gsl_vector_get(vect, 0));

	gsl_matrix_set(w, 2, 0, -gsl_vector_get(vect, 1));
	gsl_matrix_set(w, 2, 1, gsl_vector_get(vect, 0));

	return w;
}


gsl_matrix** baker_cov(const gsl_matrix *P, const gsl_matrix *w0)
{
    assert(P != NULL);
    assert(w0 != NULL);

	gsl_vector_const_view w0View = gsl_matrix_const_subcolumn(w0, 0, 0, 3);
	gsl_matrix *W0 = tensorop(&w0View.vector);

	gsl_matrix *temp = gsl_matrix_alloc(W0->size1, W0->size2);

	// A = (eye(3) + 1/2 * W0 + 1/12 * W0^2);
	// eye(3)
	gsl_matrix *A = gsl_matrix_alloc(W0->size1, W0->size2);
	gsl_matrix_set_identity(A);
	// + 1/2 * W0
	gsl_matrix_memcpy(temp, W0);
	gsl_matrix_scale(temp, 0.5);
	gsl_matrix_add(A, temp);
	// + 1/12 * W0^2
	gsl_matrix_set_zero(W0);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					1.0, W0, W0,
					0.0, temp);
	gsl_matrix_scale(temp, 1/12);
	gsl_matrix_add(A, temp);


	// S = A * P * A.';
	// .' = Array transpose (same as ' in this case?)
	gsl_matrix *S = gsl_matrix_calloc(A->size1, A->size2);
	// A * P
	gsl_matrix *tempAP = gsl_matrix_calloc(A->size1, P->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					1.0, A, P,
					0.0, tempAP);
	// * A.'
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					1.0, tempAP, A,
					0.0, S);


	//B = (1 - 1/2 * trace(S)) * eye(3)+ 1/2 * S;
	// trace(S) sums the values in the diagonal of S
	// (1 - 1/2 * trace(S))
	double traceS = 1 - 0.5 * trace(S);
	// * eye(3)
	gsl_matrix *B = gsl_matrix_alloc(S->size1, S->size2);
	gsl_matrix_set_identity(B);
	gsl_matrix_scale(B, traceS);
	// + 1/2 * S
	if (temp->size1 != S->size1 && temp->size2 != S->size2)
	{
		gsl_matrix_free(temp);
		temp = gsl_matrix_alloc(S->size1, S->size2);
	}
	gsl_matrix_memcpy(temp, S);
	gsl_matrix_scale(temp, 0.5);
	gsl_matrix_add(B, temp);

	gsl_matrix **ret = calloc(2, sizeof(gsl_matrix*));
	ret[0] = gsl_matrix_alloc(B->size1, B->size2);
	ret[1] = gsl_matrix_alloc(S->size1, S->size2);
	gsl_matrix_memcpy(ret[0], B);
	gsl_matrix_memcpy(ret[1], S);


	/*
		Clean up
	*/
	gsl_matrix_free(W0);
	gsl_matrix_free(temp);
	gsl_matrix_free(B);
	gsl_matrix_free(tempAP);
	gsl_matrix_free(S);
	gsl_matrix_free(A);


	return ret;
}


gsl_matrix* SigmaHat0(const gsl_matrix *A)
{
    assert(A != NULL);

	// [U,E] = eig(A);
	// [matlab] where E is the eigenvalues, and U whose columns are the right eigenvectors
	eigen_t eigen = eig(A);

	// kappa = diag(E);
	// [matlab] gets a column vector of the main diagonal elements of E
	// The gsl lib is already in this format. With the values in eigen.values.


	// E=eye(3) - 0.50 * diag([1/(kappa(1)+kappa(3))+1/(kappa(1)+kappa(2)) 1/(kappa(2)+kappa(3))+1/(kappa(1)+kappa(2)) 1/(kappa(1)+kappa(3))+1/(kappa(2)+kappa(3))]);
	//
	// create the diagonal matrix
	double diag[3] = {
		1 / (gsl_vector_get(eigen.values, 0) + gsl_vector_get(eigen.values, 2))
		+ 1 / (gsl_vector_get(eigen.values, 0) + gsl_vector_get(eigen.values, 1)),

		1 / (gsl_vector_get(eigen.values, 1) + gsl_vector_get(eigen.values, 2))
		+ 1 / (gsl_vector_get(eigen.values, 0) + gsl_vector_get(eigen.values, 1)),

		1 / (gsl_vector_get(eigen.values, 0) + gsl_vector_get(eigen.values, 2))
		+ 1 / (gsl_vector_get(eigen.values, 1) + gsl_vector_get(eigen.values, 2))
	};

	gsl_vector_view diagVect = gsl_vector_view_array(diag, 3);
	gsl_matrix *diagMat = gsl_matrix_calloc(3, 3);
	SetDiagonal(diagMat, &diagVect.vector);

	// * 0.5
	gsl_matrix_scale(diagMat, 0.5);

	// create identity matrix, then subtract diagMat
	gsl_matrix *E = gsl_matrix_calloc(3, 3);
	gsl_matrix_set_identity(E);
	gsl_matrix_sub(E, diagMat);


	// f = U * E * U';
	// U * E
	gsl_matrix *tempUE = gsl_matrix_calloc(eigen.vectors->size1, E->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					1.0, eigen.vectors, E,
					0.0, tempUE);

	// * U'
	gsl_matrix *f = gsl_matrix_calloc(tempUE->size1, eigen.vectors->size1);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					1.0, tempUE, eigen.vectors,
					0.0, f);

	/*
		Clean up
	*/
	gsl_matrix_free(diagMat);
	gsl_matrix_free(E);
	gsl_matrix_free(tempUE);
	eigen_t_free(&eigen);

	return f;
}


gsl_matrix** coninv0(const gsl_matrix *C)
{
    assert(C != NULL);

	gsl_matrix** ret = (gsl_matrix**)calloc(3, sizeof(gsl_matrix*));

	gsl_matrix** pa0 = polar_right(C);

	if (pa0[0] == NULL && pa0[1] == NULL)
	{
		return ret;
	}

	// returnedArray[0] = P
	// returnedArray[2] = A0
	ret[0] = pa0[0];
	ret[2] = pa0[1];

	// [U,E]=eig(A0);
	// U is a matrix of eigenvectors
	// E is a diagonal matrix of eigenvalues
	//
	// eigen_t stores the eigenvalues as a vector
	eigen_t eigen = eig(pa0[1]);

	// f = E - eye(3);
	double eye[9] = {0};
	gsl_matrix_view eyeView = gsl_matrix_view_array(eye, 3, 3);
	gsl_matrix_set_identity(&eyeView.matrix);

	double f[9] = {0};
	gsl_matrix_view fView = gsl_matrix_view_array(f, 3, 3);
	SetDiagonal(&fView.matrix, eigen.values);
	gsl_matrix_sub(&fView.matrix, &eyeView.matrix);


	// f1 = f(1,1); f2 = f(2,2); f3 = f(3,3);
	long double f1, f2, f3;
	f1 = (long double) gsl_matrix_get(&fView.matrix, 0, 0);
	f2 = (long double) gsl_matrix_get(&fView.matrix, 1, 1);
	f3 = (long double) gsl_matrix_get(&fView.matrix, 2, 2);


	/*
	k1=-(3*f1^2 - 2*f3*f1 - 2*f2*f1 + 2*f2*f3 - f3^2 - f2^2) / ((f2+f1-f3) * (-f2+f1+f3) * (-f2+f1-f3)) / 2;
	k2= (2*f2*f3 - 2*f3*f1 - 3*f2^2 + f3^2 + f1^2 + 2*f2*f1) / ((f2+f1-f3) * (-f2+f1+f3) * (-f2+f1-f3)) / 2;
	k3= (2*f2*f3 + 2*f3*f1 + f1^2 - 2*f2*f1 - 3*f3^2 + f2^2) / ((f2+f1-f3) * (-f2+f1+f3) * (-f2+f1-f3)) / 2;
	*/
	long double k1, k2, k3;
	k1 = -(3.0f*f1*f1 -  2.0f*f3*f1 -  2.0f*f2*f1 +  2.0f*f2*f3 - f3*f3 - f2*f2) / ((f2+f1-f3) * (-f2+f1+f3) * (-f2+f1-f3)) /  2.0f;
	k3 = ( 2.0f*f2*f3 -  2.0f*f3*f1 - 3.0f*f2*f2 + f3*f3 + f1*f1 +  2.0f*f2*f1) / ((f2+f1-f3) * (-f2+f1+f3) * (-f2+f1-f3)) /  2.0f;
	k2 = ( 2.0f*f2*f3 +  2.0f*f3*f1 + f1*f1 -  2.0f*f2*f1 - 3.0f*f3*f3 + f2*f2) / ((f2+f1-f3) * (-f2+f1+f3) * (-f2+f1-f3)) /  2.0f;


	// Ahat = U * diag([k1 k2 k3]) * U';
	// create diag([k1 k2 k3])
	double diagArray[9] = {0};
	gsl_matrix_view diagView = gsl_matrix_view_array(diagArray, 3, 3);
	double vectArray[3] = {(double) k1, (double) k2, (double) k3};
	gsl_vector_view vectView = gsl_vector_view_array(vectArray, 3);
	SetDiagonal(&diagView.matrix, &vectView.vector);

	// U * diag([k1 k2 k3])
	gsl_matrix *tempUDiag = gsl_matrix_calloc(eigen.vectors->size1, (&diagView.matrix)->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					1.0, eigen.vectors, &diagView.matrix,
					0.0, tempUDiag);

	// * U'
	ret[1] = gsl_matrix_calloc(tempUDiag->size1, eigen.vectors->size1);
	gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					1.0, tempUDiag, eigen.vectors,
					0.0, ret[1]);

	/*
		Clean up
	*/
	gsl_matrix_free(tempUDiag);
	eigen_t_free(&eigen);
	free(pa0);

	return ret;
}


eigen_t eig(const gsl_matrix *mat)
{
    assert(mat != NULL);
	// Example code for determining eigenvalues and eigenvectors
	// https://www.gnu.org/software/gsl/manual/html_node/Eigenvalue-and-Eigenvector-Examples.html

	eigen_t eigen = {0};

	gsl_matrix *tempMat = gsl_matrix_calloc(mat->size1, mat->size2);
	gsl_matrix_memcpy(tempMat, mat);

	eigen.values = gsl_vector_calloc(mat->size1);
	eigen.vectors = gsl_matrix_calloc(mat->size1, mat->size2);

	gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(mat->size1);

	gsl_eigen_symmv(tempMat, eigen.values, eigen.vectors, workspace);
	gsl_eigen_symmv_sort (eigen.values, eigen.vectors,
                        GSL_EIGEN_SORT_ABS_DESC);


	gsl_eigen_symmv_free(workspace);
	gsl_matrix_free(tempMat);
	return eigen;
}


double get_det(const gsl_matrix * A)
{
    assert(A != NULL);
	int sign=0; double det=0.0; int row_sq = A->size1;
	gsl_permutation * p = gsl_permutation_calloc(row_sq);
	gsl_matrix * tmp_ptr = gsl_matrix_calloc(row_sq, row_sq);
	int * signum = &sign;
	gsl_matrix_memcpy(tmp_ptr, A);
	gsl_linalg_LU_decomp(tmp_ptr, p, signum);
	det = gsl_linalg_LU_det(tmp_ptr, *signum);
	gsl_permutation_free(p);
	gsl_matrix_free(tmp_ptr);
	return det;
}


double
trace(const gsl_matrix *mat)
{
    assert(mat != NULL);

	double traceVal = 0.0;

	for (unsigned int i = 0; i < mat->size1 && i < mat->size2; i++)
	{
		traceVal += gsl_matrix_get(mat, i, i);
	}

	return traceVal;
}

gsl_matrix* DCM2Euler(const gsl_matrix* mat)
{
    assert(mat != NULL);

	if (mat->size1 != 3 || mat->size2 != 3)
	{
		return NULL;
	}

	double value;
	gsl_matrix* result = gsl_matrix_calloc(3, 1);

	// roll
	value = atan2(gsl_matrix_get(mat, 1, 2), gsl_matrix_get(mat, 2, 2));
	gsl_matrix_set(result, 0, 0, value);

	// pitch
	value = -asin(gsl_matrix_get(mat, 0, 2));
	gsl_matrix_set(result, 1, 0, value);

	// yaw
	value = atan2(gsl_matrix_get(mat, 0, 1), gsl_matrix_get(mat, 0, 0));
	gsl_matrix_set(result, 2, 0, value);

	return result;
}
