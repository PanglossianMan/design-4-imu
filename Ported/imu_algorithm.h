#ifndef IMU_ALGORITHM
#define IMU_ALGORITHM

#include <stdio.h>
#include <inttypes.h>
#include <string.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_math.h>

#include "util.h"
#include "math_util.h"

#define SAMPLE_FREQ 200 // Sample frequency (Hz)
#define LOOP_NUM_TO_PRINT 2 || i == 1 || i == 3 || i == 9

/*
	Applies the algorithm as described in MATLAB code

	Input:
		inputMatrix -	Matrix containing data to apply algorithm to.
						Each data reading is in a new row, which each column
						being representing as follows

		Time(s)	ax(g)	ay(g)	az(g)	wx(°/s)	wy(°/s)	wz(°/s)	AngleX(°)	AngleY(°)	AngleZ(°)	hx	hy	hz

		outputMatrix -	empty pointer to a matrix that will be created during
						operation.
						Will be NULL if an error occurs.

	Return:
		Returns a struct populated with resulting matricies and values.
*/
result_t ApplyAlgorithm(gsl_matrix *inputMatrix);


#endif /* IMU_ALGORITHM */
