/*
	Initial port of sophia's IMU/Sensor fusion algorithm from MATLAB to C.

	Reads in one of the provided data files and creates an output data file
	with the computed values.


	Requires: GNU Scientific library (GSL)

	Input:
		port01.c <input filename> <output filename>



	Written by: William O'Connell (s3330253)

*/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

// For profiling
#include <unistd.h>
#include <sys/time.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>

#include "util.h"
#include "imu_algorithm.h"

void PrintUsage(char *argv);

gsl_matrix* LoadData(char *filename);

unsigned long GetTime();

int
main (int argc, char **argv)
{
	if (argc != 2)
	{
		PrintUsage(*argv);
		return EXIT_SUCCESS;
	}

	char* inputFilename = argv[1];

	unsigned long startLoadTime = GetTime();
	gsl_matrix *dataMatrix = LoadData(inputFilename);

	/*
		Convert gyro readings from deg/s to rad/s
		Gyro readings are contained in columns [5:7]
	*/
	gsl_matrix_view gyroView = gsl_matrix_submatrix(dataMatrix, 0, 4, dataMatrix->size1, 3);
	gsl_matrix_scale(&gyroView.matrix, M_PI/180.0f);
	gsl_matrix *transposedDataMatrix = gsl_matrix_alloc(dataMatrix->size2, dataMatrix->size1);
	gsl_matrix_transpose_memcpy(transposedDataMatrix, dataMatrix);

	unsigned long endLoadTime = GetTime();

	FPrintMatrix("../Data/Input_data_c.txt", transposedDataMatrix);

    printf("Running algorithm...");
	unsigned long startAlgorithmTime = GetTime();
	result_t results = ApplyAlgorithm(dataMatrix);
	unsigned long endAlgorithmTime = GetTime();
    printf("Done!\n");

	printf("\nLoad time: %f\nAlgorithm time: %f\n",
		(double)(endLoadTime - startLoadTime) / 1E6,
		(double)(endAlgorithmTime - startAlgorithmTime) / 1E6);

	FPrintMatrix("../Data/Result_Euler_data_c.txt", results.EulerAngles);

	/*
		Clean up
	*/
	result_t_free(&results);
	gsl_matrix_free(transposedDataMatrix);
	gsl_matrix_free(dataMatrix);


	return EXIT_SUCCESS;
}


void
PrintUsage(char *argv)
{
	fprintf(stderr, "Error: Invalid argument\n");
	fprintf(stderr, "Usage: %s <input data file> <output data file>\n", argv);
	fprintf(stderr, "\t<input data file> - name of the file containing"
	 	" formatted input data\n");
	fprintf(stderr, "\t<output data file> - name of the file to store"
		" generated data\n");
}


/*
	Loads sensor data from file into gsl_matrix

	Input:
		filename - name of the file to read data from

	Return:
		Returns a matrix populated with the data from the input file.
		Pointer will be NULL is an error occurs creating the matrix, or opening
		the file.

	NOTE: 	May miscount the number of lines (actual - 1) if the final line of
			data does not end in a newline.
			Also requires that the data file contains only data, no headers or
			footers.


*/
gsl_matrix *
LoadData(char *filename)
{
	assert(filename != NULL);

	gsl_matrix *mat = NULL;


	FILE *fp = fopen(filename, "r");
	if (!fp)
	{
		fprintf(stderr, "Error: Unable to open data file to load\n");
	}
	else
	{
		// Find the number of lines in file
		char c;
		int numLines = 0;
		while ((c = fgetc(fp)) != EOF)
		{
			if (c == '\n')
			{
				numLines++;
			}
		}
		fseek(fp, 0, SEEK_SET);

		fprintf(stdout, "File contains %d lines\n", numLines);

		// create empty matrix
		int numColumns = 13;
		mat = gsl_matrix_alloc(numLines, numColumns);
		//gsl_matrix_set_zero(mat);

		// Read data and load into matrix
		int row = 0;
		int success;
		float val[numColumns];
		while (row < numLines)
		{
			success = fscanf(fp, "%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\n",
				&val[0],
				&val[1],
				&val[2],
				&val[3],
				&val[4],
				&val[5],
				&val[6],
				&val[7],
				&val[8],
				&val[9],
				&val[10],
				&val[11],
				&val[12]);

			if (success)
			{
				for (int i = 0; i < numColumns; i++)
				{
					gsl_matrix_set(mat, row, i, val[i]);
				}

				//fprintf(stdout, "Read line, %d items\n", err);
			}
			else
			{
				fprintf(stderr, "Error: Invalid line in file, line %d\n", row);
			}
			row++;
		}

		fprintf(stdout, "%d rows loaded from [%s]\n", row, filename);
		fclose(fp);
	}

	return mat;
}



unsigned long
GetTime()
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return (tp.tv_sec*1E6) + tp.tv_usec;
}
