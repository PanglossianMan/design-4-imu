Port of Sensor fusion algorithm from MATLAB to C
C code written by William O'Connell (s3330253)


Requires GNU Scientific Library (GSL) to build [libgsl0ldbl, libgsl0-dev]


Reads in data from a text file containing tab delimited data.

The program my be built and run using "make run". The input files may be changed by altering "INPUT_DATA_FILE"
in the makefile.

Additional Commands:
	"make debug"	- builds the program and launches gdb
	"make memtest"	- builds the program and runs program with valgrind leak
					  that will report memory leaks and any files not closed
	"make format"	- formats all source code in directory using clang-format
					  [INCOMPLETE - Not fully implemented, any changes create
					  a copy without modifying the source]

Files generated for debugging results are placed in directory "../Data/"
If this directory does not exist then the generated data will not be saved.

Last test the results from this program closely matched the values produced
by the matlab program.
Some errors with a larger difference between the results do appear but only
affect a small number of samples.
