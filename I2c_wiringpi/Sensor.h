/*
	TODO: Change the library used from wiringPi to generic linux I2C
	driver. SM bus?
*/


#pragma once

#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#ifdef USE_WIRINGPI

#include <wiringPiI2C.h>

#else /* Use smbus */

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include "i2c/smbus.h"

#endif /* END USE_WIRINGPI */

#include "MPU6050.h"

struct MPU6050Data
{
	int16_t accelX;
	int16_t accelY;
	int16_t accelZ;
	int16_t gyroX;
	int16_t gyroY;
	int16_t gyroZ;
	int16_t temp;
};

struct MPU6050DataScaled
{	
	float accelX;
	float accelY;
	float accelZ;
	float gyroX;
	float gyroY;
	float gyroZ;
	float temp;
};

struct MPU6050State
{
	uint8_t interruptStatus;
	uint16_t FIFOCount;
};



/*
	Configures the MPU6050 and enables the accelerometer and gyroscope.

	returns true if successful, false otherwise.
*/
bool MPU6050_Initialize();

/*
	Reads data from the device and loads the values into the data struct.

	returns true if successful, false otherwise.
*/
bool MPU6050_ReadData(struct MPU6050Data * dataOut);


/*
	Reads the state information from the device and loads the values into the
	state struct.

	returns true if successful, false otherwise.
*/
bool MPU6050_ReadState(struct MPU6050State * stateOut);


/*
	Scales the raw sensor values based on the module settings.
*/
void MPU6050_ScaleData(struct MPU6050Data * rawData, struct MPU6050DataScaled * scaledData);
