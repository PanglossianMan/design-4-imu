I2C_MPU6050
-----------

This program reads a sensor (MPU6050, Accel + Gyro) via I2C using either 
the wiringPi library, or using smbus functions via the i2c-tools library.

Additionally you will need to enable the I2C kernal module.

This program is for testing the libraries and their performance. Further work 
will integrate this into the main program.
Further increase in performance should be able to be acheived by changing the 
way the program reads the data from the sensor, from individual reads to a 
single block read.



To build use either...
	
	> make wiring

	or

	> make smbus


Libraries
---------
[https://github.com/WiringPi/WiringPi]

[https://github.com/groeck/i2c-tools]