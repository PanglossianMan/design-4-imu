#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <stdbool.h>
#include <string.h>

#include "Sensor.h"

#define TIME_TO_RUN_SEC 2
#define DELAY_MSEC 1

/*
	Delays by at least <msec> milliseconds.
*/
void delay(unsigned int msec)
{
	struct timeval now, before;
	double timeElapsed;

	// get start time
	gettimeofday(&before, NULL);


	do
	{
		// Get current time
		gettimeofday(&now, NULL);

		// Calculate elapsed time.
		timeElapsed = (double) (now.tv_usec - before.tv_usec) / 1000 
			+ (double) (now.tv_sec - before.tv_sec) * 1000;
	} while (timeElapsed < msec);

}

int main(int argc, char** argv)
{
	printf("Initializing sensor...\n");

	if (!MPU6050_Initialize())
	{
		printf("Failed, exiting...\n");
		exit(0);
	}

	/////////////////////////
	// Begin measurement loop
	//
	// Loop runs for TIME_TO_RUN_SEC seconds
	/////////////////////////

	struct MPU6050Data sensorVals;
	struct MPU6050DataScaled scaledSensorVals;

	// Initialize time structs for timing.
	struct timeval tvBeginRun, tvCurrent;
	gettimeofday(&tvBeginRun, NULL);

	int count = 0;

	do 
	{
		MPU6050_ReadData(&sensorVals);
		MPU6050_ScaleData(&sensorVals, &scaledSensorVals);

		count++;
		/*
		printf("%d Accel: %8f %8f %8f\tGyro: %8f %8f %8f\tTemp: %f\n", 
			count,
			scaledSensorVals.accelX, scaledSensorVals.accelY, scaledSensorVals.accelZ,
			scaledSensorVals.gyroX, scaledSensorVals.gyroY, scaledSensorVals.gyroZ,
			scaledSensorVals.temp);
		*/	
		delay(DELAY_MSEC);
		gettimeofday(&tvCurrent, NULL);
	} while (tvCurrent.tv_sec - tvBeginRun.tv_sec < TIME_TO_RUN_SEC);

	printf("%d samples recorded\n", count);


	return EXIT_SUCCESS;
}
