#include <stdlib.h>
#include <ctype.h>
#include <errno.h>

// For profiling
#include <unistd.h>
#include <sys/time.h>
#include <inttypes.h>

#include <math.h>
#include <gsl/gsl_matrix.h>

#include "arduino-serial-lib.h"
#include "SoftSensor.h"
#include "program_options.h"
#include "ParseFile.h"
#include "BinaryDataStructs.h"

#define ACCEL_SCALE_FACTOR 1.0f
#define GYRO_SCALE_FACTOR 14.375f;
#define MAG_SCALE_FACTOR 0.92f;

#define NUM_COLUMNS 9

#define SERIAL_BUFFER_SIZE 512

typedef struct
{
	SensorValues_type accel;
	SensorValues_type gyro;
	SensorValues_type mag;
} combinedValues_type;

void PrintEulerAngles(EulerAngles_type angles);

gsl_matrix* LoadData(program_settings_type options);

uint64_t GetTime();

combinedValues_type ParseString(char * str);
void ScaleValues(combinedValues_type * values);

int main(int argc, char** argv)
{
    FILE *dataLogFile = NULL;

    // Read commandline flags
    program_settings_type options = ParseOptions(argc, argv);

    if (!options.isValid)
    {
        PrintUsage(*argv);
        exit(EXIT_SUCCESS);
    }
    else
    {
        dataLogFile = fopen(options.logFilename, "w+");
        if (dataLogFile == NULL)
        {
            fprintf(stderr, "ERROR: Unable to open data log file [errno:%d]\n", errno);
            exit(-1);
        }

    	printf("Input type: ");
        fprintf(dataLogFile, "Input: ");
    	switch (options.inputType)
    	{
    		case INPUT_FILE:
    			printf("FILE\n");
                fprintf(dataLogFile, "FILE");

                printf("Sensor columns - AccelPos: %d GyroPos: %d MagPos: %d\n",
                    options.accelPos, options.gyroPos, options.magPos);
    			break;
    		case INPUT_SERIAL:
    			printf("SERIAL\n");
                fprintf(dataLogFile, "SERIAL");
    			break;
    		case INPUT_NONE:
    			printf("NONE\n");
    			break;
    		default:
    			printf("ERROR - INVALID INPUT TYPE\n");
    			break;
    	}
    	printf("Input name: %s\n", options.inputStr);
        fprintf(dataLogFile, " [%s]\n", options.inputStr);

    	printf("Number of samples: ");
        fprintf(dataLogFile, "Sample Limit: ");
    	if (options.numSamples == 0)
    	{
    		printf("continuous\n");
            fprintf(dataLogFile, "Unlimited\n");
    	}
    	else
    	{
    		printf("%d\n", options.numSamples);
            fprintf(dataLogFile, "%d\n", options.numSamples);
    	}

    	printf("Sample rate: %f Hz\n", options.sampleRate);
        fprintf(dataLogFile, "Sample Rate: %f Hz\n", options.sampleRate);

        printf("Log file: %s\n", options.logFilename);

        fprintf(dataLogFile, "\n"
        "============================================================\n\n");
    }


    /////////////////////////
    // initialise soft sensor
    /////////////////////////
    SoftSensor_type sensorAlg;
    memset(&sensorAlg, 0, sizeof(SoftSensor_type));
    SoftSensor_Init(&sensorAlg, options.sampleRate);

    SensorValues_type accelData;
    SensorValues_type gyroData;
    SensorValues_type magData;

    uint64_t startAlgorithmTime;
    uint64_t endAlgorithmTime;

    if (options.inputType == INPUT_FILE)
    {
    	printf("\n");
	    	// Open data file and load into matrix
	    gsl_matrix *dataMatrix = LoadData(options);
	    int numSamples = dataMatrix->size1;

	    if (options.numSamples == 0)
	    {
	    	options.numSamples = numSamples;
	    }
	    else if (numSamples < options.numSamples)
	    {
	    	options.numSamples = numSamples;
	    	printf("WARNING - 	number of samples from file is smaller than specified,\n");
	    	printf("\t\t\tusing value from file\n");
	    }

	    gsl_matrix_view gyroView = gsl_matrix_submatrix(dataMatrix, 0, 3, dataMatrix->size1, 3);
#ifdef GYRO_CONVERT_DEGREES_TO_RADIANS
	    /*
	        Convert gyro readings from deg/s to rad/s
	        Gyro readings are contained in columns [5:7]
	    */
	    gsl_matrix_scale(&gyroView.matrix, M_PI/180.0f);
#endif // GYRO_CONVERT_DEGREES_TO_RADIANS

#ifdef COMPENSATE_GYRO_BIAS
	    {
	    	double gxBias = 6.6 * M_PI / 180.0f;
		    double gyBias = -0.5 * M_PI / 180.0f;
		    double gzBias = -1 * M_PI / 180.0f;
		    
		    for (int i = 0; i < numSamples; ++i)
		    {
		    	// gx -= gxBias;
		    	gsl_matrix_set(&gyroView.matrix, i, 0, 
		    		gsl_matrix_get(&gyroView.matrix, i, 0) - gxBias);

		    	// gy -= gyBias;
		    	gsl_matrix_set(&gyroView.matrix, i, 1, 
		    		gsl_matrix_get(&gyroView.matrix, i, 1) - gyBias);

		    	// gz -= gzBias;
		    	gsl_matrix_set(&gyroView.matrix, i, 2, 
		    		gsl_matrix_get(&gyroView.matrix, i, 2) - gzBias);
		    }
	    }
#endif // COMPENSATE_GYRO_BIAS

	    // fetch data and run sensor algorithm
	    // FetchSensorData();
	    // SoftSensor_Update();
	    // LogSensor(SoftSensor_GetEuler());
	    startAlgorithmTime = GetTime();
	    for (int i = 0; i < options.numSamples; i++)
	    // for (int i = 0; i < 2; i++)
	    {
	        // Get current sensor values from loaded data file
	        accelData.x = gsl_matrix_get(dataMatrix, i, 0);
	        accelData.y = gsl_matrix_get(dataMatrix, i, 1);
	        accelData.z = gsl_matrix_get(dataMatrix, i, 2);
	        gyroData.x = gsl_matrix_get(dataMatrix, i, 3);
	        gyroData.y = gsl_matrix_get(dataMatrix, i, 4);
	        gyroData.z = gsl_matrix_get(dataMatrix, i, 5);
	        magData.x = gsl_matrix_get(dataMatrix, i, 6);
	        magData.y = gsl_matrix_get(dataMatrix, i, 7);
	        magData.z = gsl_matrix_get(dataMatrix, i, 8);

	        // Update sensor / Run algorithm on current values
	        SoftSensor_Update(&sensorAlg, &accelData, &gyroData, &magData);

	        // PrintEulerAngles(SoftSensor_GetEuler(&sensorAlg, ANGLE_DEG));

            // Log current angles to file
            EulerAngles_type angles = SoftSensor_GetEuler(&sensorAlg, ANGLE_DEG);
            fprintf(dataLogFile, "%8ld\t%3.12f\t%3.12f\t%3.12f\n",
                sensorAlg.numTicks, angles.roll, angles.pitch, angles.yaw);
	    }
	    endAlgorithmTime = GetTime();

	    // Print the final angles
	    PrintEulerAngles(SoftSensor_GetEuler(&sensorAlg, ANGLE_DEG));

	    // Clean up
	    gsl_matrix_free(dataMatrix);
    }	// END INPUT_FILE
    else if (options.inputType == INPUT_SERIAL)
    {
    	int fd = serialport_init(options.inputStr, 115200);
    	int err;
    	if (fd == -1)
    	{
    		fprintf(stderr, "ERROR: Unable to open serial port\n");
    	}
    	else
    	{
	    	char inputbuf[SERIAL_BUFFER_SIZE];
	    	bool hasStarted = false;
	    	int hasMissedMessage = 0;
			SensorValues_type accel, gyro, mag;
	    	for (int i = 0; i < options.numSamples; )
	    	{
		    	memset(inputbuf, 0, SERIAL_BUFFER_SIZE);

		    	// Read a line from serial port
	    		err = serialport_read_until(fd, inputbuf, "\n\n\n", SERIAL_BUFFER_SIZE, 5000);
	    		if (err < 0)
	    		{
	    			fprintf(stderr, "ERROR: Problem reading serial port\n");
	    			break;
	    		}

	    		/*
	    		printf("[");
	    		for (int ind = 0; ind < err; ind++)
	    		{
	    			printf("%c", inputbuf[ind]);
	    		}
	    		printf("]\n");
	    		*/

#ifndef SERIAL_BINARY_DATA
	    		if (strlen(inputbuf) < 5)
	    		{
	    			// Bad string, string is actually longer
	    			continue;
	    		}
	    		else if (strstr(inputbuf, "ERROR:") != NULL)
	    		{
	    			// Error occured on board
	    			fprintf(stderr, "Board error\n> %s", inputbuf);
	    		}
	    		else if (strstr(inputbuf, "Starting...\n") != NULL)
	    		{
	    			printf("XXXXXXXXXXXXXXXXXXX\n"
	    				"Starting...\n"
	    				"XXXXXXXXXXXXXXXXXXX\n");
	    			hasStarted = true;
	    		}
	    		else
	    		{
	    			if (hasStarted)
	    			{
	    				i++;
			    		printf("> %s", inputbuf);

                        // parse values from input
			    		combinedValues_type receivedVals = ParseString(inputbuf);
			    		ScaleValues(&receivedVals);

			    		SoftSensor_Update(&sensorAlg,
			    			&receivedVals.accel, &receivedVals.gyro, &receivedVals.mag);

                        // Log current angles to file
                        EulerAngles_type angles = SoftSensor_GetEuler(&sensorAlg, ANGLE_DEG);
                        fprintf(dataLogFile, "%8ld\t%3.6f\t%3.6f\t%3.6f\n",
                            sensorAlg.numTicks, angles.roll, angles.pitch, angles.yaw);
	    			}
	    		}
#else // SERIAL_BINARY_DATA is defined
		    	if (strstr(inputbuf, "ERROR:") != NULL)
	    		{
	    			// Error occured on board
	    			fprintf(stderr, "Board error\n> %s", inputbuf);
	    		}
	    		else if (strstr(inputbuf, "Starting...\n") != NULL)
	    		{
	    			printf("XXXXXXXXXXXXXXXXXXX\n"
	    				"Starting...\n"
	    				"XXXXXXXXXXXXXXXXXXX\n");
	    			hasStarted = true;
	    		}
	    		else
	    		{
	    			if (hasStarted)
	    			{
	    				if (*((uint8_t*)inputbuf) < 28
	    					|| err < 8)
				    	{
				    		// Message too short, invalid
				    		//fprintf(stderr, "Line skip\t");
		    				//printf("Length: %d\n", *((uint8_t*)inputbuf));

				    		continue;
				    	}

	    				i++;
	    				// Check message size, first byte in message
	    				//printf("Length: %d\n", *((uint8_t*)inputbuf));
	    				int count = *((int32_t*)&((uint8_t*)inputbuf)[1]);
	    				printf("Count: %d\n", count);

	    				if (i != count) 
    					{
    						//fprintf(stderr, "ERROR: Missed message [%d:%d]\n", i, count);
    						hasMissedMessage = i;
    					}
    					printf("Count: %d\n", i);

	    				// Get accel values
	    				struct AccelVals *accelData = (struct AccelVals*)(&inputbuf[5]);
	    				printf("\tAccel - X: %d Y: %d Z: %d\n",
	    					accelData->x, accelData->y, accelData->z);

	    				// Get gyro values
	    				struct GyroVals *gyroData = 
	    					(struct GyroVals*)(&inputbuf[5 + sizeof(struct AccelVals)]);
	    				printf("\tGyro - X: %f Y: %f Z: %f\n",
	    					gyroData->x, gyroData->y, gyroData->z);

	    				// Get Mag values
	    				struct MagVals *magData = 
	    					(struct MagVals*)(&inputbuf[5 + sizeof(struct AccelVals)
	    						+ sizeof(struct GyroVals)]);
	    				printf("\tMag - X: %d Y: %d Z: %d\n",
	    					magData->x, magData->y, magData->z);

	    				accel.x = accelData->x;
	    				accel.y = accelData->y;
	    				accel.z = accelData->z;
	    				gyro.x = gyroData->x;
	    				gyro.y = gyroData->y;
	    				gyro.z = gyroData->z;
	    				// Mag sometimes returns invalid/empty data
	    				// When null mag data is received, the previous sample
	    				// will be used instead.
	    				if (magData->x != 0 && magData->y != 0 && magData->z != 0)
	    				{
		    				mag.x = magData->x;
		    				mag.y = magData->y;
		    				mag.z = magData->z;
	    				}

	    				SoftSensor_Update(&sensorAlg, &accel, &gyro, &mag);
	    			}
	    		}
#endif // SERIAL_BINARY_DATA
	    	}	// END for (int i = 0; i < options.numSamples; )

	    	if (hasMissedMessage != 0) printf("Count no match\n");

	    	serialport_close(fd);

			// Print the final angles
		    PrintEulerAngles(SoftSensor_GetEuler(&sensorAlg, ANGLE_DEG));
		}

    }
    else
    {
    	fprintf(stderr, "ERROR: No input selected\n");
    }

    // Cleanup
    SoftSensor_Destroy(&sensorAlg);
    fclose(dataLogFile);

    // Profiling info
    printf("\nAlgorithm time: %f secs over %d samples\n",
		(double)(endAlgorithmTime - startAlgorithmTime) / 1E6, options.numSamples);
    printf("%f secs per sample (avg)\n",
        (double)(endAlgorithmTime - startAlgorithmTime) / 1E6 / options.numSamples);

    return EXIT_SUCCESS;
}

void PrintEulerAngles(EulerAngles_type angles)
{
    printf("X: %3.4f\tY: %3.4f\tZ: %3.4f\n",
        angles.roll, angles.pitch, angles.yaw);
}


/*
	Loads sensor data from file into gsl_matrix

	Input:
		filename - name of the file to read data from

	Return:
		Returns a matrix populated with the data from the input file.
		Pointer will be NULL is an error occurs creating the matrix, or opening
		the file.

	NOTE: 	May miscount the number of lines (actual - 1) if the final line of
			data does not end in a newline.
			Also requires that the data file contains only data, no headers or
			footers.


*/
gsl_matrix *
LoadData(program_settings_type options)
{
	assert(options.inputStr != NULL);

	gsl_matrix *mat = NULL;


	FILE *fp = fopen(options.inputStr, "r");
	if (!fp)
	{
		fprintf(stderr, "Error: Unable to open data file to load\n");
	}
	else
	{
        int numLines = GetNumLines(fp);

		fprintf(stdout, "File contains %d lines\n", numLines);

		// Read data and load into matrix
		// expects 3 values for each sensor
		SensorValues_type *accelValues = calloc(numLines, sizeof(SensorValues_type));
		SensorValues_type *gyroValues = calloc(numLines, sizeof(SensorValues_type));
		SensorValues_type *magValues = calloc(numLines, sizeof(SensorValues_type));

		//SkipLines(fp, HEADER_LINES);

		int currentLine = 0;
		bool magValueError = false;

        while (NUM_COLUMNS == ParseLine(fp,
            options.accelPos, options.gyroPos, options.magPos,
            &accelValues[currentLine], &gyroValues[currentLine], &magValues[currentLine]))
		{
			// If invalid mag values are read in,
			// use a previous sample (if available)
			// Only mag values are copied
			if (magValues[currentLine].x == 0.0
				&& magValues[currentLine].y == 0.0
				&& magValues[currentLine].z == 0.0)
			{
				if (!magValueError)
				{
					magValueError = true;
					fprintf(stderr, "WARNING: Invalid mag value at sample %d\n", currentLine);
					fprintf(stderr, "\t Using previous sample values.\n");
					fprintf(stderr, "\t (This message will only appear once)\n");
				}
				if (currentLine > 0)
				{
					// copy previous sample into current sample (mag values only)
					magValues[currentLine] = magValues[currentLine - 1];
				}
				else
				{
					// Fatal error, no valid samples to copy
					fprintf(stderr, "ERROR: No valid samples to use. Exiting...\n");
					exit(-1);
				}
			}

			currentLine++;
		}

        // Copy values into data matrix
        mat = gsl_matrix_calloc(currentLine, NUM_COLUMNS);
        for (int i = 0; i < currentLine; ++i)
        {
            gsl_matrix_set(mat, i, 0, accelValues[i].x);
            gsl_matrix_set(mat, i, 1, accelValues[i].y);
            gsl_matrix_set(mat, i, 2, accelValues[i].z);
            gsl_matrix_set(mat, i, 3, gyroValues[i].x);
            gsl_matrix_set(mat, i, 4, gyroValues[i].y);
            gsl_matrix_set(mat, i, 5, gyroValues[i].z);
            gsl_matrix_set(mat, i, 6, magValues[i].x);
            gsl_matrix_set(mat, i, 7, magValues[i].y);
            gsl_matrix_set(mat, i, 8, magValues[i].z);
        }

		fprintf(stdout, "%d rows loaded from [%s]\n", currentLine, options.inputStr);

		/*
			Clean up
		*/
		free(accelValues);
		free(gyroValues);
		free(magValues);
		fclose(fp);
	}

	return mat;
}



uint64_t
GetTime()
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return (tp.tv_sec*1E6) + tp.tv_usec;
}


combinedValues_type ParseString(char * str)
{
	combinedValues_type values;

	// example string:
	// Accel - X: 20  Y: 16  Z: -277  Gyro - X: 24.00  Y: -2.00  Z: 8.00  Mag - X: -218  Y: -53  Z: -617

	char * token;
	int tokenNum = 1;
	token = strtok(str, ":"); // This loads str into strtok and skips first token
	while ((token = strtok(NULL, ":")) != NULL)
	{
		if (token == NULL)
		{
			fprintf(stderr, "ERROR: Bad token\n");
			break;
		}

		switch (tokenNum)
		{
		case 0:
			// No interesting values
			break;
		case 1:
			values.accel.x = (float)atoi(token);
			break;
		case 2:
			values.accel.y = (float)atoi(token);
			break;
		case 3:
			values.accel.z = (float)atoi(token);
			break;
		case 4:
			values.gyro.x = (float)atoi(token);
			break;
		case 5:
			values.gyro.y = (float)atoi(token);
			break;
		case 6:
			values.gyro.z = (float)atoi(token);
			break;
		case 7:
			values.mag.x = (float)atoi(token);
			break;
		case 8:
			values.mag.y = (float)atoi(token);
			break;
		case 9:
			values.mag.z = (float)atoi(token);
			break;
		default:
			fprintf(stderr, "ERROR: Bad token num\n");
		}
		tokenNum++;
	}

	return values;
}

void ScaleValues(combinedValues_type * values)
{
	assert(values != NULL);

	// Scale accelerometer
	// Neccessary???
	values->accel.x *= ACCEL_SCALE_FACTOR;
	values->accel.y *= ACCEL_SCALE_FACTOR;
	values->accel.z *= ACCEL_SCALE_FACTOR;

	// Scale gyroscope and convert to rad/sec
	values->gyro.x *= GYRO_SCALE_FACTOR;
	values->gyro.y *= GYRO_SCALE_FACTOR;
	values->gyro.z *= GYRO_SCALE_FACTOR;

	values->gyro.x *= M_PI / 180.0f;
	values->gyro.y *= M_PI / 180.0f;
	values->gyro.z *= M_PI / 180.0f;

	// Scale Magnetometer
	values->mag.x *= MAG_SCALE_FACTOR;
	values->mag.x *= MAG_SCALE_FACTOR;
	values->mag.x *= MAG_SCALE_FACTOR;
}
