#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "SoftSensor.h"

#define STR_BUFFER_SIZE 512


/*
	Tokenizes the current line to read sensor values.
	Assumes that the 3 values for each sensor are consecutive, and that the 
	columns are tab or space delimited.

	Params:
		fd 			- The file pointer to an open file
		accelPos	- The index of the column for the first value for each
		gyroPos		  sensor. 
		magPos		
		accelValues	- Structure to be loaded with the read values for each
		gyroValues	  sensor.
		magValues

		Return		- The number of sensor values read.

	WARNING: Does not validate that a particular value is correct or NaN
*/
int ParseLine(FILE* fp, int accelPos, int gyroPos, int magPos, 
	SensorValues_type* accelVals, SensorValues_type* gyroVals, SensorValues_type* magVals);

/*
	Moves the file pointer ahead N lines,
	where N is 'numLinesToSkip'.
*/
void SkipLines(FILE* fd, int numLinesToSkip);

/*
	Returns the number of lines in the file.
*/
int GetNumLines(FILE* fd);