/*
    Loads Functions to read in commandline options and set the corresponding
    flags in the program_settings_type struct.
*/

#pragma once

#include <getopt.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define PROG_OPT_DEFAULT_LOG_FILE "./log.txt"

typedef enum {INPUT_NONE, INPUT_FILE, INPUT_SERIAL} input_type;

typedef struct
{
	// Wheter the options parsed were valid
	bool isValid;

    //
	// Data input source.
    //
	input_type inputType;
	const char* inputStr;
    
    char *posStr;
    int accelPos;
    int gyroPos;
    int magPos;

    //
    // Data options
    //
	// The number of samples to process before stopping
	// 0 = continuous until EOF received or user exits program
	int numSamples;

	// the rate at which the samples are taken, used in SoftSensor
	float sampleRate;

    //
    // Logging options
    //
    const char* logFilename;
} program_settings_type;


void PrintUsage(char *argv);

program_settings_type ParseOptions(int argc, char** argv);
