#include "ParseFile.h"

int ParseLine(FILE* fd, int accelPos, int gyroPos, int magPos, 
	SensorValues_type* accelVals, SensorValues_type* gyroVals, SensorValues_type* magVals)
{
	assert(fd != NULL);
	assert(accelVals != NULL);
	assert(gyroVals != NULL);
	assert(magVals != NULL);

	// stats
	int numLines = 0;

	char strBuf[STR_BUFFER_SIZE];
	int pos = 0;

	// Read line from file
	int c;
	while ((char)(c = getc(fd)) != '\n' && c != EOF)
	{
		strBuf[pos] = (char)c;
		pos++;
	}

	// Parse line

	strBuf[pos] = '\0';
	numLines++;


	// Parse line
	char* tok = strtok(strBuf, " \t\n");
	int tokNum = 0;
	int valCount = 0;
	while (tok != NULL)
	{
		// Read if accel value
		switch (tokNum - accelPos)
		{
		case 0:
			accelVals->x = atof(tok);
			valCount++;
			break;
		case 1:
			accelVals->y = atof(tok);
			valCount++;
			break;
		case 2:
			accelVals->z = atof(tok);
			valCount++;
			break;

		default:
			break;
		}

		// Read if Gyro value
		switch (tokNum - gyroPos)
		{
		case 0:
			gyroVals->x = atof(tok);
			valCount++;
			break;
		case 1:
			gyroVals->y = atof(tok);
			valCount++;
			break;
		case 2:
			gyroVals->z = atof(tok);
			valCount++;
			break;

		default:
			break;
		}

		// Read if Mag value
		switch (tokNum - magPos)
		{
		case 0:
			magVals->x = atof(tok);
			valCount++;
			break;
		case 1:
			magVals->y = atof(tok);
			valCount++;
			break;
		case 2:
			magVals->z = atof(tok);
			valCount++;
			break;

		default:
			break;
		}

		// get next token
		tok = strtok(NULL, " \t\n");
		tokNum++;
	}

	return valCount;
}


void SkipLines(FILE* fd, int numLinesToSkip)
{
	assert(fd != NULL);

	while (numLinesToSkip > 0)
	{
		char c = getc(fd);
		if ((int) c == EOF)
		{
			fprintf(stderr, "ERROR: Unable to skip lines, at end of file.");
			break;
		}
		if (c == '\n') numLinesToSkip--;
	}
}


int GetNumLines(FILE* fd)
{
	assert(fd != NULL);

	// Record the current position in file
	long currentPos = ftell(fd);

	// Find the number of lines in file
	int c;
	int numLines = 1;
	while ((c = fgetc(fd)) != EOF)
	{
		if ((char)c == '\n')
		{
			numLines++;
		}
	}

	// Reset pointer to recorded position
	fseek(fd, currentPos, SEEK_SET);

	return numLines;
}
