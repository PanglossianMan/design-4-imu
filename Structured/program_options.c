#include "program_options.h"

void
PrintUsage(char *argv)
{
	fprintf(stderr, "Error: Invalid argument\n");
	fprintf(stderr, "Usage: %s ...\n", argv);
	fprintf(stderr, "\t -f <input data file> - name of the file containing"
	 	" formatted input data\n");
	fprintf(stderr, "\t or\n");
	fprintf(stderr, "\t -s <input serial port> - name of the serial port"
	 	" with incoming data\n");
	fprintf(stderr, "\t -n <number of samples> - Specify the number of samples"
		" to read before stopping, 0 to continue until EOF is read\n");
    fprintf(stderr, "\t -r <sample rate> - Specify the rate at which the samples"
        " were taken in Hz (ie. 200)\n");
    fprintf(stderr, "\t -l <data log file> - [Optional] Specifies the log file for"
        " the program, all data generated will recorded here."
        " If not set default log file will be used (log.txt).\n");
}


program_settings_type ParseOptions(int argc, char** argv)
{
	program_settings_type options;
	options.isValid = false;
	options.inputType = INPUT_NONE;
    options.logFilename = NULL;

	// check if any arguments have been passed.
	if (argc < 2)
	{
		options.isValid = false;
		return options;
	}

    int opt;
    while ((opt = getopt(argc, argv, "f:s:n:r:l:d:")) != -1)
    {
        switch (opt)
        {
        case 'f': // FILE input
            if (options.inputType != INPUT_NONE)
            {
                options.isValid = false;
                return options;
            }

            options.inputType = INPUT_FILE;
            options.inputStr = optarg;
            break;
        case 's': // Serial port input
            if (options.inputType != INPUT_NONE)
            {
                options.isValid = false;
                return options;
            }

            options.inputType = INPUT_SERIAL;
			options.inputStr = optarg;
            break;
        case 'n': // Num samples
            // check that there is number to read
            if (isdigit(optarg[0]))
            {
                options.numSamples = atoi(optarg);
            }
            else
            {
                options.isValid = false;
                return options;
            }
            break;
        case 'r': // data sample rate
            // check that there is number to read
            if (isdigit(optarg[0]))
            {
                options.sampleRate = atof(optarg);
            }
            else
            {
                options.isValid = false;
                return options;
            }
            break;
        case 'l': // Log file (output)
            options.logFilename = optarg;
            break;
        case 'd': // sensor data location (for file input)
            // Expected format is "a:g:m", where a, g and m are the
            // column numbers that the data for the accel, gyro and mag are
            // located in in the input file.

            options.posStr = optarg;
            // tokenize string
            // Get accel position
            char *currentToken = strtok(options.posStr, ":");
            if (currentToken == NULL)
            {
                fprintf(stderr, "ERROR: Invalid Accel position string");
                options.isValid = false;
                return options;
            }
            options.accelPos = atoi(currentToken);

            // Get gyro position
            currentToken = strtok(NULL, ":");
            if (currentToken == NULL)
            {
                fprintf(stderr, "ERROR: Invalid Gyro position string");
                options.isValid = false;
                return options;
            }
            options.gyroPos = atoi(currentToken);

            // Get mag position
            currentToken = strtok(NULL, ":");
            if (currentToken == NULL)
            {
                fprintf(stderr, "ERROR: Invalid Mag position string");
                options.isValid = false;
                return options;
            }
            options.magPos = atoi(currentToken);

            // fprintf(stdout, "AccelPos: %d GyroPos: %d MagPos: %d\n",
            //     options.accelPos, options.gyroPos, options.magPos);

            break;
        case '?':
            // Invalid argument
            options.isValid = false;
            return options;
        }
    }

    if (options.logFilename == NULL) options.logFilename = PROG_OPT_DEFAULT_LOG_FILE;
	options.isValid = true;
	return options;
}
