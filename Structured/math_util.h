#ifndef MATH_UTIL_H
#define MATH_UTIL_H

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>

#include "util.h"

// Returns two gsl_matrix* in as array of size 2
// If an error occurs each will be NULL.
//
// returnedArray[0] = R
// returnedArray[1] = A
gsl_matrix** polar_left(const gsl_matrix *mat);

// Returns two gls-matrix* as an array of size 2
// If an error occurs each will be NULL.
// This array must be freed
//
// returnedArray[0] = R
// returnedArray[1] = A
gsl_matrix** polar_right(const gsl_matrix *mat);

// Returns gsl_matrix with result
// mat must be a 1x3 matrix
gsl_matrix* tensorop(const gsl_vector *vect);

// Returns two gsl_matrix* as array of size 2
// This array must be freed
//
// returnedArray[0] = B
// returnedArray[1] = S
gsl_matrix** baker_cov(const gsl_matrix *P, const gsl_matrix *w0);

gsl_matrix* SigmaHat0(const gsl_matrix *A);

// Returns an array of gsl_matrix* of size 3
// If an error occurs each will be null
// This array must be freed
//
// returnedArray[0] = P
// returnedArray[1] = Ahat
// returnedArray[2] = A0
gsl_matrix** coninv0(const gsl_matrix *C);

// Determines the eigenvalues and eigenvectors for the vectors in mat
//
// The returned struct contains a gsl_vector* with the eigenvalues,
// and a gsl_matrix* with the eigenvectors.
eigen_t eig(const gsl_matrix *mat, bool sort);

// Returns the determinant of the matrix A
// Using the code from:
// https://lists.gnu.org/archive/html/help-gsl/2005-02/msg00013.html
double get_det(const gsl_matrix * A);

double trace(const gsl_matrix *mat);

// Converts the Direction cosine matrix into euler angles
//
// mat must be a [3x3] matrix
//
// returns a [3x1] matrix with roll pitch yaw
// in row 0 to 2 respectively
gsl_matrix* DCM2Euler(const gsl_matrix* mat);


#endif /* MATH_UTIL_H */
