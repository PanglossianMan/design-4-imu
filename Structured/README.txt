C code written by William O'Connell (s3330253)
Based off of Matlab algorithm written by Sophia Suvorova

Requires
	> GNU Scientific Library (GSL) to build [libgsl0ldbl, libgsl0-dev]


Reads in data from a text file containing tab delimited data. (To be updated in the future)

The program my be built and run using "make run". The input files may be changed by altering "INPUT_DATA_FILE"
in the makefile.

Additional Make Commands:
	"make debug"	- builds the program and launches gdb
	"make memtest"	- builds the program and runs program with valgrind leak
					  that will report memory leaks and any files not closed

This program in a restructured version of the MATLAB algorithm port.
Any Results produced by this program should match the ported program results,
and closely represent the values produced by the initial MATLAB program.

A simple program using the algorithm is shown in 'main.c', the LoadData() function
may need to be replaced if different input data is used.

The program now has some options that may be changed via command line arguments
passed when running the program. These change the input data and allow a simplified
method to change some settings when running the algorithm.


This program has a number of command line arguments to control the behaviour
without requiring a recompilation.

Command line args:
	NOTE: -f and -s can not be selected at the same time.
	-f <input data file>
		Path to data file containing sensor data. File is expected to be 
		white-space delimited with no column headers.

	-s <input serial port>
		The name/path of the serial port data to be received from. Each sample
		is expected to be whitespace delimited on a single line.

	-n <number of samples>
		The number of samples to evaluate before exiting. Program will exit 
		early if an EOF character is received or there are fewer samples in the 
		data file. 0 will continue indefinitely until EOF.

	-r <sample rate>
		The sample rate of the data in Hz or samples/sec (i.e 200). 
		This may be a decimal value.

	-d <sensor data location>
			NOTE: 	Only works for data file input.
					The string must be of the format "a:g:m", where 'a' is the first
					column for accelerometer values, 'g' is the first column for 
					gyroscope values, and 'm' if the first column for magnetometer values.
					Columns are numbered starting at 0.
					e.g. -d 0:4:9 for "ax ay az data gx gy gz data data mx my mz"
			Specify which columns in the data file represent each sensor.

	-l <data log file>
		[Optional] Specify an alternate path/filename for the log file. If no
		log file is specified "log.txt" will be generated in the same path as 
		the executable.
		This will overwrite a file with the same name if it already exists in
		the specified path.


If using serial port and binary data, data must be formatted as 
	|(uint8_t) message size | data block | '\n' |

TODO:
    Update how algorithm time is calculated for serial port.
