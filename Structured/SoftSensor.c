#include "SoftSensor.h"

void SoftSensor_Init(SoftSensor_type * state, double sampleFreq)
{
    assert(state != NULL);
    assert(sampleFreq > 0.0);

    state->sampleFrequency = sampleFreq;

	// Acceleration of gravity
	// g = [0, 0, 1];
	gsl_matrix *g = gsl_matrix_calloc(3, 1);
	gsl_matrix_set(g, 2, 0, 1);

	// magnetic field for Melbourne
	// h = [21313.3;
	//		4388.4;
	//		55947.2]
	gsl_matrix *magFieldMatrix = gsl_matrix_alloc(3, 1);
	gsl_matrix_set(magFieldMatrix, 0, 0, 21313.3);
	gsl_matrix_set(magFieldMatrix, 1, 0, 4388.4);
	gsl_matrix_set(magFieldMatrix, 2, 0, 55947.2);

	// Normalise magnetic field (matrix h0)
	gsl_vector_view magFieldVectorView = gsl_matrix_column(magFieldMatrix, 0);
	double magFieldNorm = gsl_blas_dnrm2(&magFieldVectorView.vector);
	gsl_matrix_scale(magFieldMatrix, 1 / magFieldNorm);

	// D
	double dArray[] = { 1000.0f, 10.0f };
	gsl_vector_view dVectorView = gsl_vector_view_array(dArray, 2);
	state->D = gsl_matrix_calloc(2, 2);
	SetDiagonal(state->D, &dVectorView.vector);

	/*
	X matrix
	*/
	state->X = gsl_matrix_calloc(3, 2);
	gsl_vector_view gravityVectorView = gsl_matrix_column(g, 0);
	double gravityNorm = gsl_blas_dnrm2(&gravityVectorView.vector);
	gsl_vector_view magFieldVectorView2 = gsl_matrix_column(magFieldMatrix, 0);
	double magFieldNorm2 = gsl_blas_dnrm2(&magFieldVectorView2.vector);
	gsl_matrix_set_col(state->X, 0, &gravityVectorView.vector);
	gsl_matrix_set_col(state->X, 1, &magFieldVectorView2.vector);
	gravityVectorView = gsl_matrix_column(state->X, 0);
	magFieldVectorView2 = gsl_matrix_column(state->X, 1);
	gsl_vector_scale(&gravityVectorView.vector, 1 / gravityNorm);
	gsl_vector_scale(&magFieldVectorView2.vector, 1 / magFieldNorm2);

	// Qd
	state->Qd = gsl_matrix_calloc(6, 6);
	gsl_matrix_set_identity(state->Qd);
	gsl_matrix_view qdView = gsl_matrix_submatrix(state->Qd, 0, 0, 3, 3);
	gsl_matrix_scale(&qdView.matrix, 1E-4);
	qdView = gsl_matrix_submatrix(state->Qd, 3, 3, 3, 3);
	gsl_matrix_scale(&qdView.matrix, 1E-2);


	// Qm
	state->Qm = gsl_matrix_calloc(3, 3);
	gsl_matrix_set_identity(state->Qm);

	// P
	state->P = gsl_matrix_calloc(6, 6);
	gsl_matrix_set_identity(state->P);

	// H
	state->H = gsl_matrix_calloc(3, 6);
	gsl_matrix_set_identity(state->H);

	// F
	state->F = gsl_matrix_calloc(6, 6);
	gsl_matrix_set_identity(state->F);

	gsl_matrix_view fView = gsl_matrix_submatrix(state->F, 0, 3, 3, 3);
	gsl_matrix_set_identity(&fView.matrix);
	gsl_matrix_scale(&fView.matrix, 1 / sampleFreq);


    state->R = gsl_matrix_calloc(3, 3);
    state->A = gsl_matrix_calloc(3, 3);
    state->state = gsl_matrix_calloc(6, 1);

    /*
        Clean up
    */
    gsl_matrix_free(g);
    gsl_matrix_free(magFieldMatrix);
}

void SoftSensor_Update(
    SoftSensor_type * sensorState,
    SensorValues_type * accelValues,
    SensorValues_type * gyroValues,
    SensorValues_type * magValues)
{
    assert(sensorState != NULL);
    assert(accelValues != NULL);
    assert(gyroValues != NULL);
    assert(magValues != NULL);

	if (sensorState->numTicks == 0)
	{
		/*
		Normalized Y-axis accel and mag values
		Y = [acc(:, 1) / norm(acc(:, 1)), mgn(:, 1) / norm(mgn(:, 1))];
		*/
		// Find norm of Y-axis accel values
		double accelArray[] = { accelValues->x, accelValues->y, accelValues->z };
		gsl_vector_view accelView = gsl_vector_view_array(accelArray, 3);
		double accelNorm = gsl_blas_dnrm2(&accelView.vector);
		// 0.9623
		// Find norm of Y-axis mag values
		double magArray[] = { magValues->x, magValues->y, magValues->z };
		gsl_vector_view magView = gsl_vector_view_array(magArray, 3);
		double magNorm = gsl_blas_dnrm2(&magView.vector);

		// size of matrix is number of axis (rows), by accel and mag readings (columns)
		gsl_matrix *y = gsl_matrix_alloc(3, 2);
		// copy accel and mag values into y matrix
		gsl_matrix_set_col(y, 0, &accelView.vector);
		gsl_matrix_set_col(y, 1, &magView.vector);
		// create mat view of accel and mag values, change view from input matrix to
		// y matrix
		accelView = gsl_matrix_column(y, 0);
		magView = gsl_matrix_column(y, 1);
		// scale accel and mag values by their norm
		gsl_vector_scale(&accelView.vector, 1 / accelNorm);
		gsl_vector_scale(&magView.vector, 1 / magNorm);


		/*
		[RotationS03.m:51] polar_left(X*D*Y')
		Product of X, D, and transpose of Y.
		*/
		// X*D*Y'
		// Do X*D with temp X matrix
		gsl_matrix *xd = gsl_matrix_calloc(sensorState->X->size1, sensorState->D->size2);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, sensorState->X, sensorState->D,
					   0.0, xd);
		// Do (X*D) * Y'
		// create array for 0's
		gsl_matrix *out = gsl_matrix_calloc(xd->size1, y->size1);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					   1.0, xd, y,
					   0.0, out);

		gsl_matrix **ra = polar_left(out);
        gsl_matrix_memcpy(sensorState->R, ra[0]);
        gsl_matrix_memcpy(sensorState->A, ra[1]);

		// State
		gsl_matrix_set(sensorState->state, 0, 0, -gyroValues->x);
		gsl_matrix_set(sensorState->state, 1, 0, -gyroValues->y);
		gsl_matrix_set(sensorState->state, 2, 0, -gyroValues->z);


        /*
            Clean up
        */
        gsl_matrix_free(y);
        gsl_matrix_free(xd);
        gsl_matrix_free(out);
        gsl_matrix_free(ra[0]);
        gsl_matrix_free(ra[1]);
        free(ra);
	}
	else
	{
		/*
			Alloc temp vars
		*/
        gsl_matrix *y = gsl_matrix_alloc(3, 2);
        gsl_matrix *xd = gsl_matrix_calloc(sensorState->X->size1, sensorState->D->size2);
        gsl_matrix *out = gsl_matrix_calloc(xd->size1, y->size1);
		gsl_matrix *statep = gsl_matrix_alloc(sensorState->F->size1, sensorState->state->size2); // [6x1]

		gsl_matrix *pp = gsl_matrix_alloc(6, 6);
		gsl_matrix *tempFP = gsl_matrix_alloc(6, 6);
		gsl_matrix *tempFPFT = gsl_matrix_alloc(6, 6);

		gsl_matrix *s = gsl_matrix_alloc(3, 3);
		gsl_matrix *tempHPP = gsl_matrix_alloc(3, 6);
		gsl_matrix *tempHPPHT = gsl_matrix_alloc(3, 3);

		gsl_matrix *k = gsl_matrix_alloc(6, 3);
		gsl_matrix *tempPPHT = gsl_matrix_alloc(6, 3);
		gsl_matrix *tempInvS = gsl_matrix_alloc(3, 3);

		gsl_matrix *w = gsl_matrix_alloc(3, 1);
		gsl_matrix *tempHStatep = gsl_matrix_alloc(3, 1);

		// gsl_matrix *tempKW = gsl_matrix_alloc(6, 1);

		gsl_matrix *tempEyeKH = gsl_matrix_alloc(6, 6);

		gsl_matrix *omega = NULL;

		gsl_matrix *w0 = gsl_matrix_alloc(3, 3);

		gsl_matrix *a0 = gsl_matrix_alloc(sensorState->A->size1, sensorState->A->size2);

		gsl_matrix *tempB0W0 = gsl_matrix_alloc(3, 3);
		gsl_matrix *tempB0W0Sigma = gsl_matrix_alloc(3, 3);
		gsl_matrix *tempB0W0SigmaR = gsl_matrix_alloc(3, 3);
		gsl_matrix **coninv0Result = NULL;


		/*
			Start
		*/
		// Y = [acc(:,i) / norm(acc(:,i))   mgn(:,i) / norm(mgn(:,i))];
		double accArray[] = { accelValues->x, accelValues->y, accelValues->z };
		double magArray[] = { magValues->x, magValues->y, magValues->z };
		gsl_vector_view accColumnView = gsl_vector_view_array(accArray, 3);
		gsl_vector_view mgnColumnView = gsl_vector_view_array(magArray, 3);
        double gyroArray[] = { gyroValues->x, gyroValues->y, gyroValues->z };
        gsl_matrix_view gyroView = gsl_matrix_view_array(gyroArray, 3, 1);


		double accNorm = gsl_blas_dnrm2(&accColumnView.vector);
		gsl_vector_scale(&accColumnView.vector, 1 / accNorm);

		double mgnNorm = gsl_blas_dnrm2(&mgnColumnView.vector);
		gsl_vector_scale(&mgnColumnView.vector, 1 / mgnNorm);

		gsl_matrix_set_col(y, 0, &accColumnView.vector);
		gsl_matrix_set_col(y, 1, &mgnColumnView.vector);

		// [R0] = polar_left(X * D * Y');
		// X*D*Y'
		// Do X*D with temp X matrix
		gsl_matrix_set_zero(xd);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, sensorState->X, sensorState->D,
					   0.0, xd);
		// Do (X*D) * Y'
		// create array for 0's
		gsl_matrix_set_zero(out);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					   1.0, xd, y,
					   0.0, out);
		gsl_matrix **r0 = polar_left(out);


		// statep = F * state;
		gsl_matrix_set_zero(statep);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, sensorState->F, sensorState->state,
					   0.0, statep);


		// Pp = F * P * F' + Qd;
		// (F * P)
		gsl_matrix_set_zero(tempFP);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, sensorState->F, sensorState->P,
					   0.0, tempFP);
		// * F'
		gsl_matrix_set_zero(tempFPFT);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					   1.0, tempFP, sensorState->F,
					   0.0, tempFPFT);
		// + Qd
		gsl_matrix_memcpy(pp, tempFPFT);
		gsl_matrix_add(pp, sensorState->Qd);



		// S = H * Pp * H' + Qm;
		// S is the 3x3 top left submatrix of Pp + Qm
		// H * Pp
		gsl_matrix_set_zero(tempHPP);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, sensorState->H, pp,
					   0.0, tempHPP);
		// * H'
		gsl_matrix_set_zero(tempHPPHT);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					   1.0, tempHPP, sensorState->H,
					   0.0, tempHPPHT);
		// + Qm
		gsl_matrix_memcpy(s, tempHPPHT);
		gsl_matrix_add(s, sensorState->Qm);


		// K = Pp * H' / S
		//   = Pp * H' * S^-1;
		// If S is always diagonal, The values in S^-1 are the inverse of
		// those in S
		// NOTE: S is assumed to be a diagonal matrix
		if (CheckIfDiag(s))
		{
			// Pp * H'
			gsl_matrix_set_zero(tempPPHT);
			gsl_blas_dgemm(CblasNoTrans, CblasTrans,
						   1.0, pp, sensorState->H,
						   0.0, tempPPHT);
			// create S^-1 from S
			gsl_matrix_memcpy(tempInvS, s);
			gsl_matrix_set(tempInvS, 0, 0, 1 / gsl_matrix_get(s, 0, 0));
			gsl_matrix_set(tempInvS, 1, 1, 1 / gsl_matrix_get(s, 1, 1));
			gsl_matrix_set(tempInvS, 2, 2, 1 / gsl_matrix_get(s, 2, 2));

			// (Pp * H') * S^-1
			gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
						   1.0, tempPPHT, tempInvS,
						   0.0, k);
		}
		else
		{
			fprintf(stderr, "[loop count = %ld] S is not diagonal, exiting...\n",
                sensorState->numTicks);
			exit(-1);
		}

		// w = -gyro(:,i) - H * statep;
		//
		// -gyro(:,i)
		gsl_matrix_memcpy(w, &gyroView.matrix);
		gsl_matrix_scale(w, -1.0);
		// H * Statep
		gsl_matrix_set_zero(tempHStatep);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, sensorState->H, statep,
					   0.0, tempHStatep);
		// -gyro(:,i) - H * Statep
		gsl_matrix_sub(w, tempHStatep);
		// state = statep + K * w;
		// K * w
		gsl_matrix_set_zero(sensorState->state);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, k, w,
					   0.0, sensorState->state);
		// + statep
		gsl_matrix_add(sensorState->state, statep);


		// P = (eye(6) - K * H) * Pp;
		// eye(6) - K * H
		gsl_matrix_set_identity(tempEyeKH);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   -1.0, k, sensorState->H,
					   1.0, tempEyeKH);
		// * Pp
		gsl_matrix_set_zero(sensorState->P);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, tempEyeKH, pp,
					   0.0, sensorState->P);



		// Omega = tensorop(state(1:3));
		gsl_vector_view tempVectView = gsl_matrix_subcolumn(sensorState->state, 0, 0, 3);
		omega = tensorop(&tempVectView.vector);



		// W0 = expm(Omega / f);
		//
		// NOTE: This function is officially unsupported (as far as I can tell).
		// and is hence not in the documentation. See the following link.
		// https://lists.gnu.org/archive/html/help-gsl/2009-12/msg00032.html
		gsl_matrix_scale(omega, 1 / sensorState->sampleFrequency);
		gsl_linalg_exponential_ss(omega, w0, 0.01);



		// B0 = baker_cov(P(1:3,1:3) / f^2, state(1:3) / f);
		// The following creates temporary copies of P and state, then appropriately
		// multiplies (divides) them before applying baker_cov.
		// P(1:3, 1:3) / f^2
		double pTempMat[36] = { 0 };
		gsl_matrix_view pTempMatView = gsl_matrix_view_array(pTempMat, 6, 6);
		gsl_matrix_memcpy(&pTempMatView.matrix, sensorState->P);
		gsl_matrix_view pTempSubMat = gsl_matrix_submatrix(&pTempMatView.matrix, 0, 0, 3, 3);
		gsl_matrix_scale(&pTempSubMat.matrix,
            1 / (sensorState->sampleFrequency * sensorState->sampleFrequency));
		// state(1:3) / f
		double stateTempMat[6] = { 0 };
		gsl_matrix_view stateTempMatView = gsl_matrix_view_array(stateTempMat, 6, 1);
		gsl_matrix_memcpy(&stateTempMatView.matrix, sensorState->state);
		gsl_matrix_view stateTempSubMat = gsl_matrix_submatrix(&stateTempMatView.matrix, 0, 0, 3, 1);
		gsl_matrix_scale(&stateTempSubMat.matrix, 1 / sensorState->sampleFrequency);
		// baker_cov()
		gsl_matrix **bs = baker_cov(&pTempSubMat.matrix, &stateTempSubMat.matrix);



		// A0 = A
		gsl_matrix_memcpy(a0, sensorState->A);



		// [R,A]=coninv0(B0 * W0 * SigmaHat0(A0) * R);
		// create temp R
		double tempRArray[9] = { 0 };
		gsl_matrix_view tempRView = gsl_matrix_view_array(tempRArray, 3, 3);
		gsl_matrix_memcpy(&tempRView.matrix, sensorState->R);
		// free ra
		gsl_matrix_free(sensorState->R);
		gsl_matrix_free(sensorState->A);
		//free(ra);
		// B0 * W0
		gsl_matrix_set_zero(tempB0W0);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, bs[0], w0,
					   0.0, tempB0W0);
		// * Sigmahat0(A0)
		gsl_matrix *sighat = SigmaHat0(a0);
		gsl_matrix_set_zero(tempB0W0Sigma);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, tempB0W0, sighat,
					   0.0, tempB0W0Sigma);
		// * R
		gsl_matrix_set_zero(tempB0W0SigmaR);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, tempB0W0Sigma, &tempRView.matrix,
					   0.0, tempB0W0SigmaR);


		// [R,A] = coninv0()
		coninv0Result = coninv0(tempB0W0SigmaR);
		sensorState->R = coninv0Result[0];
		sensorState->A = coninv0Result[1];
		coninv0Result[0] = NULL;
		coninv0Result[1] = NULL;


		// [R,A] = polar_left(X * D * Y' + R' * A);
		// X*D*Y'
		// Do X*D with temp X matrix
		gsl_matrix_set_zero(xd);
		gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,
					   1.0, sensorState->X, sensorState->D,
					   0.0, xd);
		// Do (X*D) * Y'
		// create array for 0's
		gsl_matrix_set_zero(out);
		gsl_blas_dgemm(CblasNoTrans, CblasTrans,
					   1.0, xd, y,
					   0.0, out);

		// R' * A
		double tempRAArray[9] = { 0 };
		gsl_matrix_view tempRAView = gsl_matrix_view_array(tempRAArray, 3, 3);
		gsl_blas_dgemm(CblasTrans, CblasNoTrans,
					   1.0, sensorState->R, sensorState->A,
					   0.0, &tempRAView.matrix);
		// X * D * Y' + R' * A
		gsl_matrix_add(out, &tempRAView.matrix);
		// [R, A] = polar_left()
		gsl_matrix** polarLeftResult = polar_left(out);

		gsl_matrix_memcpy(sensorState->R, polarLeftResult[0]);
		gsl_matrix_memcpy(sensorState->A, polarLeftResult[1]);


		/*
		Clean up
		*/
		gsl_matrix_free(polarLeftResult[1]);
		gsl_matrix_free(polarLeftResult[0]);
		free(polarLeftResult);
		if (coninv0Result[0] != NULL)
		{
			gsl_matrix_free(coninv0Result[0]);
			coninv0Result[0] = NULL;
		}
		if (coninv0Result[1] != NULL)
		{
			gsl_matrix_free(coninv0Result[1]);
			coninv0Result[1] = NULL;
		}
		//gsl_matrix_free(ra[0]);
		//gsl_matrix_free(ra[1]);
		gsl_matrix_free(coninv0Result[2]);
		coninv0Result[2] = NULL;
		free(coninv0Result);
		coninv0Result = NULL;
		gsl_matrix_free(sighat);
		sighat = NULL;
		gsl_matrix_free(bs[0]);
		gsl_matrix_free(bs[1]);
		free(bs);
		bs = NULL;
		gsl_matrix_free(omega);
		omega = NULL;
		gsl_matrix_free(r0[0]);
		gsl_matrix_free(r0[1]);
		free(r0);
		r0 = NULL;

		/*
		Clean up
		*/
		if (coninv0Result != NULL)
		{
			if (coninv0Result[2] != NULL) gsl_matrix_free(coninv0Result[2]);
			if (coninv0Result[1] != NULL) gsl_matrix_free(coninv0Result[1]);
			if (coninv0Result[0] != NULL) gsl_matrix_free(coninv0Result[0]);
			free(coninv0Result);
		}
		gsl_matrix_free(tempB0W0SigmaR);
		gsl_matrix_free(tempB0W0Sigma);
		gsl_matrix_free(tempB0W0);
		gsl_matrix_free(a0);
		gsl_matrix_free(w0);
		gsl_matrix_free(omega);
		gsl_matrix_free(tempEyeKH);
		gsl_matrix_free(tempHStatep);
		gsl_matrix_free(w);
		gsl_matrix_free(tempInvS);
		gsl_matrix_free(tempPPHT);
		gsl_matrix_free(k);
		gsl_matrix_free(tempHPPHT);
		gsl_matrix_free(tempHPP);
		gsl_matrix_free(s);
		gsl_matrix_free(tempFPFT);
		gsl_matrix_free(tempFP);
		gsl_matrix_free(pp);
		gsl_matrix_free(statep);
        gsl_matrix_free(xd);
        gsl_matrix_free(out);
        gsl_matrix_free(y);
	}
	sensorState->numTicks++;
}

EulerAngles_type SoftSensor_GetEuler(SoftSensor_type * state, angle_format format)
{
    assert(state != NULL);

	EulerAngles_type angles;
	gsl_matrix *anglesMat = DCM2Euler(state->R);

    switch(format)
    {
    case ANGLE_RAD:
        angles.roll = gsl_matrix_get(anglesMat, 0, 0);
        angles.pitch = gsl_matrix_get(anglesMat, 1, 0);
        angles.yaw = gsl_matrix_get(anglesMat, 2, 0);
        break;

    case ANGLE_DEG:
        angles.roll = gsl_matrix_get(anglesMat, 0, 0) * 180 / M_PI;
    	angles.pitch = gsl_matrix_get(anglesMat, 1, 0) * 180 / M_PI;
    	angles.yaw = gsl_matrix_get(anglesMat, 2, 0) * 180 / M_PI;
        break;

    default:
        printf("ERROR: Invalid format selected\n");
        exit(-1);
    }

	gsl_matrix_free(anglesMat);
	return angles;
}


void SoftSensor_Destroy(SoftSensor_type * sensorState)
{
    assert(sensorState != NULL);

    gsl_matrix_free(sensorState->D);
    gsl_matrix_free(sensorState->X);
    gsl_matrix_free(sensorState->Qd);
    gsl_matrix_free(sensorState->Qm);
    gsl_matrix_free(sensorState->P);
    gsl_matrix_free(sensorState->H);
    gsl_matrix_free(sensorState->F);
    gsl_matrix_free(sensorState->R);
    gsl_matrix_free(sensorState->A);
    gsl_matrix_free(sensorState->state);
}
