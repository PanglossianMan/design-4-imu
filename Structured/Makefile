SRC = main.c SoftSensor.c util.c math_util.c program_options.c ParseFile.c
OBJ := $(SRC:.c=.o)

# Data file with any headers removed, should only contain data
# INPUT_DATA_FILE = ../Ported/matt_input_pitch\(modified\).txt
# INPUT_DATA_FILE = ./matt_input_1\(modified\).txt
# INPUT_DATA_FILE = ./matt_input_roll\ \(modified\).txt
INPUT_DATA_FILE = ./10_dof_all_data\ \[105644\].txt

# For all matt_input_... data files
# PROG_ARGS = -f $(INPUT_DATA_FILE) -d 1:4:10 -n 0 -r 200
# For 10_dof_all_data... data file
PROG_ARGS = -f $(INPUT_DATA_FILE) -d 1:4:9 -n 0 -r 125 
# For serial input
# PROG_ARGS = -s /dev/ttyACM0 -n 200 -r 100

#
# Compiler and linker settings
#
CC = gcc
CFLAGS = -Wall -Wextra -std=c99 -pedantic -ggdb -c
CFLAGS += -Iarduino-serial
CFLAGS += -DCOMPENSATE_GYRO_BIAS	# Only turn on when required, values currently hardcoded
#CFLAGS += -DGYRO_CONVERT_DEGREES_TO_RADIANS	# Converts gyro values from degrees to radians
# CFLAGS += -DSERIAL_BINARY_DATA	# Data received over serial is in binary format
LFLAGS = -lgsl -lgslcblas -lm -L./arduino-serial -lserial
EXE = a.out

#
# Source files and Compiler flags for building arduino-serial library
#
SERIAL_SRC = ./arduino-serial/arduino-serial-lib.c
SERIAL_OBJ := $(SERIAL_SRC:.c=.o)
SERIAL_CFLAGS = -Wall -Wextra -std=gnu99 -pedantic -c



all: compile link

compile: libserial
	@echo ------------
	@echo Compiling...
	@echo ------------
	gcc $(CFLAGS) $(SRC)


link:
	@echo ----------
	@echo Linking...
	@echo ----------
	gcc $(OBJ) $(LFLAGS) -o $(EXE)



run: all
	@echo ----------
	@echo Running...
	@echo ----------
	./$(EXE) $(PROG_ARGS)

debug: all
	@echo ----------------
	@echo Launching GDB...
	@echo ----------------
	gdb --args ./$(EXE) $(PROG_ARGS)


memtest: all
	@echo ---------------------
	@echo Launching valgrind...
	@echo ---------------------
	valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --track-origins=yes --track-fds=yes ./$(EXE) $(PROG_ARGS)


# Build arduino-serial into static lib
libserial:
	@echo ---------------------
	@echo Building arduino-serial-lib...
	@echo ---------------------
	$(CC) $(SERIAL_CFLAGS) $(SERIAL_SRC) -o $(SERIAL_OBJ)
	ar rcs arduino-serial/libserial.a $(SERIAL_OBJ)


clean:
	rm *.o ./$(EXE)
	rm ./arduino-serial/*.o ./arduino-serial/*.a
