#pragma once

#include <assert.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_math.h>

#include "util.h"
#include "math_util.h"

typedef enum { ANGLE_RAD, ANGLE_DEG } angle_format;

typedef struct
{
	double roll;
	double pitch;
	double yaw;
} EulerAngles_type;

/*
	Contains the state of the soft sensor.
	This should initialised using the provided methods.
*/
typedef struct
{
	// Persistant values
	double sampleFrequency;

    // initialised in init
	gsl_matrix *D;
	gsl_matrix *X;
	gsl_matrix *Qd;
	gsl_matrix *Qm;
    gsl_matrix *P;
	gsl_matrix *H;
	gsl_matrix *F;

    // initailised on first update
    gsl_matrix *R;
    gsl_matrix *A;
    gsl_matrix *state;


	// Other
	long numTicks;			// The number of times the sensor has been updated.
} SoftSensor_type;

/*
	Contains the values from the sensor for each axis.
*/
typedef struct
{
	double x;
	double y;
	double z;
} SensorValues_type;


/*
	Initialises the sensor for use.

    sensor      - sensor state to initialise
    sampleFreq  - Sample frequency of sensor data
*/
void SoftSensor_Init(SoftSensor_type *sensor, double sampleFreq);

/*
	Updates the soft sensor from hard sensor values.

    NOTE:   accelValues must be in g (ms^-2), gyroValues must be in rad/sec,
            magValues can be raw sensor values.
*/
void SoftSensor_Update(SoftSensor_type *sensor, SensorValues_type *accelValues,
    SensorValues_type *gyroValues, SensorValues_type *magValues);

/*
	Fetches the current attitude as euler angles in either degrees or radians
*/
EulerAngles_type SoftSensor_GetEuler(SoftSensor_type *sensor, angle_format format);

/*
    Destroy the sensor, releases used memory.
*/
void SoftSensor_Destroy(SoftSensor_type *sensor);
